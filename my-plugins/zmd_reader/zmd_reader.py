import http.client
import json
import os
import re
import urllib
from bs4 import BeautifulSoup
from logging import warning, info
from pelican import signals
from pelican.readers import BaseReader
from pelican.utils import pelican_open, slugify


class ZmdReader(BaseReader):
    enabled = True
    file_extensions = ['zmd']

    def _images_dl_and_replace(self, html, folder):
        # First, find all img tag
        # Link must start with "http"
        # Then, download all images to local folder
        # Finally, replace all links with their local versions

        soup = BeautifulSoup(html, 'html.parser')
        images = soup.find_all('img')
        root_path ='/images/uploaded/' + folder + '/'
        if not os.path.exists(self.settings.get('PATH') + root_path):
            os.makedirs(self.settings.get('PATH') + root_path)
        for i in images:
            src = i['src']
            if src.startswith('http'):
                ext = src.split('.')[-1]
                name = slugify(i.get('alt', src.split('/')[-1])) + '.' + ext
                path =  self.settings.get('PATH') + root_path + name
                if os.path.isfile(path):
                    info('zmd_reader: Skip download of {}, file exists already!'.format(path))
                else:
                    info('zmd_reader: Downloading {} to {}'.format(src, path))
                    try:
                        urllib.request.urlretrieve(src, path)
                    except Exception as e:
                        warning('Error while downloading image {} ({})'.format(src, e))
                i['src'] = '.' + root_path + name

        return soup.prettify()

    def _zmdtohtml(self, md):
        zmd_hostname = os.environ.get('ZMD_HOSTNAME', 'localhost')
        connection = http.client.HTTPConnection(zmd_hostname, 27272)
        headers = {'Content-type': 'application/json'}
        content = {'md': md}
        json_content = json.dumps(content)

        connection.request('POST', '/html', json_content, headers)

        response = connection.getresponse()
        return json.loads(response.read().decode())[0]

    def read(self, filename):
        with pelican_open(filename) as fp:
            text = list(fp.splitlines())

        # Split metadata from content
        metadata = {}
        for i, line in enumerate(text):
            meta_match = re.match(r'^([a-zA-Z_-]+):(.*)', line)
            if meta_match:
                name = meta_match.group(1).lower()
                value = meta_match.group(2).strip()
                metadata[name] = self.process_metadata(name, value)
            else:
                content = '\n'.join(text[i:])
                break

        output = self._zmdtohtml(content)
        output = self._images_dl_and_replace(output, metadata['slug'])

        return output, metadata


def add_reader(readers):
    for ext in ZmdReader.file_extensions:
        readers.reader_classes[ext] = ZmdReader


def register():
    signals.readers_init.connect(add_reader)
