# Usage with docker

```bash
# ZMarkdown converter (nodejs)
docker build -t docker-zmd -f zmd.Dockerfile .
docker run --name zmd-server -p 27272:27272 docker-zmd

# Pelican server
docker build -t docker-pelican -f pelican.Dockerfile .
docker run --name pelican --mount type=bind,source="$(pwd)",target=/app -p 8000:8000 docker-pelican

# Let's run everyone at once!
docker-compose up
```
