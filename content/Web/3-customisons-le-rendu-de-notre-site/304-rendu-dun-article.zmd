Title: Rendu d'un article
Authors: Eskimon
Date: 2018-10-27 09:00
Modified: 2018-10-27 09:00
Slug: tuto-pelican-304-rendu-dun-article
Tags: python,pelican,tuto
Licence: CC BY-NC-SA
Category: Web/C - Customisons le rendu de notre site
Previous: tuto-pelican-303-page-de-«-listing-»
Next: tuto-pelican-305-tp-page-daccueil
Description: Le coeur de notre site, les articles. Personnalisons les avec notre thème Pelican.

Nous y voila, une des dernières pages à créer, celle qui représentera un article !

Avec tout ce que nous avons vu, vous allez voir que c'est vraiment un jeu d'enfant !

Tout le code que nous allons écrire ici sera à mettre dans le fichier `article.html`.

Voici ce vers quoi nous allons tendre :

![Rendu d'un article](/media/galleries/5008/fc96cf5d-2526-4696-95c6-0a5e95391404.png)

# Sommaire

# Principe

## Structure

Afin de réaliser ce morceau de la page, nous allons diviser le travail en trois parties. Tout d'abord l’entête de l'article, qui sera dans une balise `<header>`. Ensuite viendra le corps de l'article, puis enfin un pied de page dans une balise `<footer>`. Le tout sera encapsulé dans une balise `<section>` et viendra personnaliser le bloc `content`. Le tout est basé sur le squelette habituel, `base.html`.

Sur le principe on aura donc quelque chose comme ceci :

```html
{% extends "base.html" %}


{% block content %}
<section>
    <header>
        <!-- L’entête de l'article -->
    </header>
    <hr>
    <div>
        <!-- Le contenu de l'article lui-même -->
    </div>
    <hr>
    <footer>
        <!-- Le pied de page de l'article -->
    </footer>
</section>
{% endblock %}
```

Bien entendu, ce n'est qu'une proposition parmi tant d'autres !

## Ajout de données dans le `<head>` de la page

Nos articles ayant plein d'informations à faire passer, nous allons rajouter des choses dans la section `<head>` de la page elle-même, comme par exemple une balise `<meta>` avec le résumé de l'article ou encore avec les tags qui définissent ce dernier. Pour cela, on va simplement personnaliser le bloc `extra_head` qui est justement prévu pour ça.

```html
{% block extra_head %}
    <link href='https://cdn.jsdelivr.net/npm/boxicons@1.5.1/css/boxicons.min.css' rel='stylesheet'>
    
    {% if article.description %}
    <meta name="description" content="{{ article.summary }}" />
    {% endif %}

    {% for tag in article.tags %}
    <meta name="tags" content="{{ tag }}" />
    {% endfor %}
{% endblock %}
```

Comme vous pouvez le voir, j'ai surtout ajouté trois choses.

Tout d'abord, j'importe le CSS de [boxicons](https://boxicons.com/), qui me permettra d'afficher des icônes assez simplement.

Ensuite, je crée une balise `<meta>` `description`, qui contiendra la variable `article.summary`. Cela permet au moteur de recherche de savoir quoi afficher dans leur page de résultat en dessous du titre de votre article (le texte sous l'url sur cette image issue du moteur de recherche qwant).

![Description dans un moteur de recherche](https://zestedesavoir.com/media/galleries/5008/7e68f1e5-4646-47e2-8b9c-5a428a698a22.png)

Enfin, j'ajoute autant de balises `<meta>` `tag` que nécessaire grâce à une boucle `for` sur la variable `article.tags`. Ceci est notamment fait pour optimiser le SEO de notre article.

*[SEO]: Search Engine Optimization

Une fois tout cela fait, attaquons l'article en lui-même !

# L'en-tête de notre article

Pour commencer, nous allons composer l'en-tête de notre article qui servira à afficher un fil d’Ariane (pour voir (et accéder) facilement la catégorie de l'article), puis viendra le titre de l'article et quelques métadonnées qui le compose.

![En-tête article](https://zestedesavoir.com/media/galleries/5008/84ac9284-8373-4b63-b220-2610010eaaf6.png)

C'est relativement trivial à faire, en voici le code que je vous explique ensuite :

```html
<header>
    <p class="breadcrumb">
        <a href="/{{ article.category.url }}">{{ article.category}}</a>
        /
        <span>{{ article.title}}</span>
    </p>
    <h1>{{ article.title}}</h1>
    <div class="metadata">
        <p>
            {% for author in article.authors %}
            <a href="/{{ author.url }}" rel="author">{{ author }}</a>
            {% endfor %}
            <time datetime="{{ article.date.isoformat() }}" pubdate>
                le {{ article.locale_date }}
            </time>
        </p>
        <p>
            {% for tag in article.tags %}
            <a class="link-item" href="/{{ tag.url }}">{{ tag }}</a></li>
            {% endfor %}
        </p>
    </div>
</header>
```

Comme vous pouvez le constater, il n'y a rien de très compliqué ici. On fait appel aux différentes variables de `article` pour composer les différentes éléments. Ensuite, via des boucles on peut afficher des listes d'éléments comme tout les auteurs ou tout les tags.

Voici le CSS utilisé (qui sera aussi repris pour d'autres morceaux de cette page) :

[[secret]]
| ```css
| section.article header a {
|   color: #007bff;
|   text-decoration: none;
| }
| 
| section.article header h1 {
|   font-size: 40px;
|   margin: 10px;
|   text-align: center;
| }
| 
| .metadata {
|   display: flex;
|   align-items: center;
|   justify-content: space-between;
| }
| 
| .metadata p {
|   display: inline-block;
| }
| 
| .link-item {
|   display: inline-block;
|   color: #007bff;
|   border: solid 1px #007bff;
|   text-decoration: none;
|   border-radius: .25rem;
|   padding: 5px;
|   margin: 0 5px;
| }
| 
| .link-item:hover {
|   color: #fff;
|   background-color: #007bff;
| }
| ```

# Le corps et le pied de l'article

Ces deux morceaux aussi seront assez simple, voyons cela...

## Le corps de l'article

Voici la partie qui représente tout le contenu de votre article et pourtant, ce sera la plus courte de ce tuto !

En effet, tout le corps de l'article se trouve dans la variable `article.content`. Il nous faut donc juste l'afficher et hop, c'est réglé !

```
<div class="article-content">
    {{ article.content }}
</div>
```

Bien entendu, tout cela est personnalisable à grand coup de CSS !

## Le pied de page

Il ne reste plus qu'à faire un morceau contenant le pied de page de notre article. En l’occurrence, j'ai choisi d'y afficher une invitation au partage, en mettant des liens vers les réseaux sociaux les plus populaires. Ces liens permettent de pré-remplir un message à poster sur les réseaux.

![Pied de page de l'article](https://zestedesavoir.com/media/galleries/5008/9a3a0f49-116c-4d40-bc95-af614e27790b.png)

```html
<footer>
    <span>Vous aimez cet article ? Partagez-le !</span>
    <a class="link-item" href="https://twitter.com/share?url={{ SITEURL }}/{{ article.url }}&amp;text={{ article.title }} - {{ SITEURL }}/{{ article.url }}" rel="nofollow" title="Partager cet article sur Twitter">
        <i class="bx bxl-twitter bx-md"></i>
    </a>
    <a class="link-item" href="https://www.facebook.com/sharer.php?u={{ SITEURL }}/{{ article.url }}&amp;t={{ article.title }} - {{ SITEURL }}/{{ article.url }}" rel="nofollow" title="Partager cet article sur Facebook">
        <i class="bx bxl-facebook bx-md"></i>
    </a>
    <a class="link-item" href="https://plus.google.com/share?url={{ SITEURL }}/{{ article.url }}&amp;hl=fr" rel="nofollow" title="Partager cet article sur Google +">
        <i class="bx bxl-google-plus bx-md"></i>
    </a>
    <a class="link-item" href="mailto:?subject={{ article.title }}&amp;body={{ SITEURL }}/{{ article.url }}"rel="nofollow" title="Partager cet article par email">
        <i class="bx bx-envelope bx-md"></i>
    </a>
</footer>
```

C'est ici que nous ferons appel aux icônes "boxicon" dont on a intégré le CSS dans le `<head>` du site.

Et un petit bout de CSS pour la forme !
[[secret]]
| 
| ```css
| footer {
|   display: flex;
|   align-items: center;
|   justify-content: center;
|   color: #000;
| }
| ```

## Intégralité de la page

En résumé, voici le code de tout mon fichier `article.html` :

[[secret]]
| ```html
| {% extends "base.html" %}
| 
| 
| {% block extra_head %}
|     <link href='https://cdn.jsdelivr.net/npm/boxicons@1.5.1/css/boxicons.min.css' rel='stylesheet'>
|     
|     {% if article.description %}
|     <meta name="description" content="{{ article.summary }}" />
|     {% endif %}
| 
|     {% for tag in article.tags %}
|     <meta name="tags" content="{{ tag }}" />
|     {% endfor %}
| {% endblock %}
| 
| 
| {% block content %}
| <section class="article">
|     <header>
|         <p class="breadcrumb">
|             <a href="/{{ article.category.url }}">{{ article.category}}</a>
|             /
|             <span>{{ article.title}}</span>
|         </p>
|         <h1>{{ article.title}}</h1>
|         <div class="metadata">
|             <p>
|                 {% for author in article.authors %}
|                 <a href="/{{ author.url }}" rel="author">{{ author }}</a>
|                 {% endfor %}
|                 <time datetime="{{ article.date.isoformat() }}" pubdate>
|                     le {{ article.locale_date }}
|                 </time>
|             </p>
|             <p>
|                 {% for tag in article.tags %}
|                 <a class="link-item" href="/{{ tag.url }}">{{ tag }}</a></li>
|                 {% endfor %}
|             </p>
|         </div>
|     </header>
|     <hr>
|     <div class="article-content">
|         {{ article.content }}
|     </div>
|     <hr>
|     <footer>
|         <span>Vous aimez cet article ? Partagez-le !</span>
|         <a class="link-item" href="https://twitter.com/share?url={{ SITEURL }}/{{ article.url }}&amp;text={{ article.title }} - {{ SITEURL }}/{{ article.url }}" rel="nofollow" title="Partager cet article sur Twitter">
|             <i class="bx bxl-twitter bx-md"></i>
|         </a>
|         <a class="link-item" href="https://www.facebook.com/sharer.php?u={{ SITEURL }}/{{ article.url }}&amp;t={{ article.title }} - {{ SITEURL }}/{{ article.url }}" rel="nofollow" title="Partager cet article sur Facebook">
|             <i class="bx bxl-facebook bx-md"></i>
|         </a>
|         <a class="link-item" href="https://plus.google.com/share?url={{ SITEURL }}/{{ article.url }}&amp;hl=fr" rel="nofollow" title="Partager cet article sur Google +">
|             <i class="bx bxl-google-plus bx-md"></i>
|         </a>
|         <a class="link-item" href="mailto:?subject={{ article.title }}&amp;body={{ SITEURL }}/{{ article.url }}"rel="nofollow" title="Partager cet article par email">
|             <i class="bx bx-envelope bx-md"></i>
|         </a>
|     </footer>
| </section>
| {% endblock %}
| ```

# Annexe : Ajouter une section de commentaires avec Disqus

Vous vous souvenez, tout au début de ce tuto je vous expliquais qu'un site statique était gravé dans le marbre etc MAIS que en fait ce n'était pas complètement exact ? Et bien voilà un excellent exemple de comment rajouter de l'interactivité à votre site : En rajoutant une section de commentaires !

Nous n'allons bien entendu pas gérer les commentaires nous-même, sinon on perdrait tout l'intérêt d'utiliser un site statique (avec 0 base de données etc). Nous allons plutôt déléguer cela à un service externe, en l’occurrence [Disqus](https://disqus.com/). Ce service est très répandu, vous l'avez sûrement déjà croisé lors de vos voyages sur internet.

Ainsi, en déléguant la gestion des commentaires à ce service, il nous suffira juste de rajouter un peu de javascript sur notre site pour intégrer ces derniers. Le chargement se fera alors en tâche de fond lors du chargement de la page, de manière transparente pour l'utilisateur.

[[information]]
| Info pour les libristes qui me lisent : Disqus est un logiciel propriétaire géré par une entreprise. Je n'ai pas connaissance d'équivalent libre.

## Créer un compte Disqus

Pour commencer il va falloir un compte sur le site Disqus. Nous pourrons ensuite y créer un nouveau site à gérer.

Pour créer un compte, aller sur la page de [*signup*](https://disqus.com/profile/signup/) et remplissez-y le formulaire de création de compte. Si vous voulez un minimum d'utilisation de vos données, laissez la dernière case décochée.

![Formulaire de création de compte Disqus](https://zestedesavoir.com/media/galleries/5008/ec5573be-eacc-46bf-9f87-2274201b8c6f.png)

Une page vous demandant ce que vous souhaitez faire apparaît, sélectionnez alors "I want to install Disqus on my site". On vous demandera alors sur une nouvelle page le nom du site à créer, saisissez-y vos informations.

![Ajouter un site dans Disqus](https://zestedesavoir.com/media/galleries/5008/e6aaa786-be3a-4885-b460-4e507a417394.png)

Il faut maintenant sélectionner un tarif, nous prendrons l'option ~offensive~ gratuite, "Free". Puis sur la page suivante, sélectionnez la toute dernière option "I don't see my platform listed, install manually with Universal Code".

Le site vous montre alors un laïus sur comment intégrer Disqus à vos page web, voici la même chose mais en français !

## Intégrer Disqus aux pages

Pour que la zone de commentaires puisse se charger, il va falloir rajouter un morceau de javascript. Nous allons mettre ce dernier juste après le `<footer>` de notre article, afin que les commentaires s'affichent à cet endroit.

Le morceau à rajouter aura cet allure :

```html
<!-- disqus start -->
<div id="disqus_thread"></div>
<script>
    var disqus_config = function () {
        this.page.url = '{{ SITEURL }}/{{ article.url }}';
        this.page.identifier = '{{ article.slug }}';
        this.page.title = '{{ article.title }}';
    };
    (function () { // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        s.src = 'https://<le-nom-de-votre-site-dans-disqus>.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the
    <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a>
</noscript>
<!-- disqus end -->
```

[[attention]]
| Faites bien attention à bien remplacer le nom de votre site à la ligne 11 du code précédent. C'est grâce à cela que le javascript fera le lien avec votre site !

Et voilà, c'est en fait tout ce qu'il y avait à faire ! Maintenant le service de commentaires se chargera automagiquement sous chacun des articles !

Si vous retournez sur la page Disqus que nous avons laissée, vous verrez que le site vous propose d'aller configurer des options supplémentaires, n'hésitez pas à aller le faire !

## Afficher un compteur de commentaires

Afin de voir en un clin d’œil combien de commentaires ont été posté sous un article, nous allons afficher un compteur de commentaires directement dans le `<header>` de l'article.

![En-tête de l'article avec nombre de commentaires](https://zestedesavoir.com/media/galleries/5008/492614d5-a96e-40dc-8d33-8a3333ea4903.png)

Pour ce faire, on commence par rajouter un lien qui servira à accueillir le texte contenant le nombre de commentaires :

```html
(<a href="{{ SITEURL }}/{{ article.url }}#disqus_thread" data-disqus-identifier="{{ article.slug }}"></a>)
```

(Dans l'exemple ci-dessus j'ai mis ce lien juste après la balise `<time>` du header de l'article)

Peu importe le texte du lien, Disqus le modifiera pour afficher la bonne information.

Ensuite, il faut de nouveau ajouter un bout de javascript qui fera une requête chez Disqus pour obtenir le comptage :

```html
{% block extra_script %}
<script id="dsq-count-scr" src="//<le-nom-de-votre-site-dans-disqus>.disqus.com/count.js" async></script>
{% endblock %}
```

De nouveau il faut bien penser à changer le nom du site disqus à charger dans le lien.

Et avez-vous remarqué ? J'ai rajouté ce morceau de javascript dans le bloc `extra_head`, qui est juste avant la balise fermante `<body>`. Ainsi ce javascript ne se chargera qu'en dernier, sans bloquer le reste de la page pour les connexions les plus lentes.

Enfin, retournons chez Disqus pour modifier le texte à afficher. En effet, Disqus est un service anglais avant tout, et donc les textes sont en anglais par défaut. Donc le texte « 0 commentaire » afficherait par défaut « *0 Comment* ». Pas top pour les sites francophones. Pour modifier ce paramètre, allez sur la page d'administration « *community* » de votre site (https://<le-nom-de-votre-site-dans-disqus>.disqus.com/admin/settings/community/). Vous verrez alors la première section qui permet de modifier les textes et les pluriels. De mon côté, je les ai modifié de la manière suivante :

![Texte des commentaires](https://zestedesavoir.com/media/galleries/5008/8e2b6223-087f-4d16-8fdd-76875c1dc9e0.png)

Une fois cela fait, vous avez tous les paramètres standards pour avoir des commentaires aux petits oignons, et un site déjà plein de supers fonctionnalités !

C'est fini, notre site est maintenant prêt à l'emploi. Enfin presque ! Il reste tout de même à faire une page d'accueil, celle que verrons en premier vos lecteurs avide de lecture de votre prose. C'est le sujet du chapitre suivant, qui ne sera rien d'autre qu'un exercice de mise en pratique de tout ce que vous avez pu apprendre dans cette partie.

