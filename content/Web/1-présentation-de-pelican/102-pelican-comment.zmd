Title: Pelican : comment ?
Authors: Eskimon
Date: 2018-10-27 09:00
Modified: 2018-10-27 09:00
Slug: tuto-pelican-102-pelican-comment
Tags: python,pelican,tuto
Licence: CC BY-NC-SA
Category: Web/A - Présentation de Pelican
Previous: tuto-pelican-101-pelican-pourquoi
Next: tuto-pelican-201-hello-world
Description: Voyons comment faire pour installer et utiliser basiquement Pelican.

Dans ce court chapitre, je vous présente la manière la plus simple pour installer Pelican.

[[attention]]
| Étant donné que c'est un logiciel en Python, les manipulations décrites ci-dessous sont facilement transposables à n'importe quel OS. Je décrirai ici comment le faire sur un système Linux, Ubuntu en particulier.

[[i]]
| Cette partie part du principe que vous connaissez l'outil de gestion de paquets python `pip` et que vous savez l'utiliser de manière simple (installer des paquets, les supprimer...)

# Sommaire

# Pip install

[[s]]
|
| **TL;DR**
| 
| `pip install pelican Markdown` ;)

Installer Pelican est vraiment trivial, puisqu'il suffit d'installer seulement deux paquets, `pelican` et `Markdown`. Comme nous ne sommes pas des gros bourrins, on va toutefois travailler proprement dans un environnement virtuel.

Je vais donc commencer par créer ce dernier puis l'activer. Je travaillerai dans le dossier "mon-super-site".

```bash
$ mkdir mon-super-site
$ cd mon-super-site
$ virtualenv venv
$ source venv/bin/activate
$ pip intall pelican Markdown
```

Et voilà, Pelican est maintenant installé !!

# Bonjour le monde

Maintenant que les outils sont là, vérifions que tout marche avant de nous lancer à l'assaut de notre propre site !

Pour cela, Pelican met à disposition un outil de démarrage sous la forme d'une commande : `pelican-quickstart`.

Lorsque vous lancez cette dernière, une série de questions vous sera posée. Ignorez celles où un choix par défaut entre crochet est proposé en vous contentant d'appuyer sur entrée et répondez n'importe quoi aux autres, nous supprimerons tout puis reviendrons sur tout cela dès le chapitre suivant. 

Un exemple de réponse peut-être le suivant :

```plain
Welcome to pelican-quickstart v3.7.1.

This script will help you create a new Pelican-based website.

Please answer the following questions so this script can generate the files
needed by Pelican.

    
> Where do you want to create your new web site? [.] 
> What will be the title of this web site? Mon super site
> Who will be the author of this web site? Moi
> What will be the default language of this web site? [fr] 
> Do you want to specify a URL prefix? e.g., http://example.com   (Y/n) 
> What is your URL prefix? (see above example; no trailing slash) http://example.com
> Do you want to enable article pagination? (Y/n) 
> How many articles per page do you want? [10] 
> What is your time zone? [Europe/Paris] 
> Do you want to generate a Fabfile/Makefile to automate generation and publishing? (Y/n) 
> Do you want an auto-reload & simpleHTTP script to assist with theme and site development? (Y/n) 
> Do you want to upload your website using FTP? (y/N) 
> Do you want to upload your website using SSH? (y/N) 
> Do you want to upload your website using Dropbox? (y/N) 
> Do you want to upload your website using S3? (y/N) 
> Do you want to upload your website using Rackspace Cloud Files? (y/N) 
> Do you want to upload your website using GitHub Pages? (y/N) 
Done. Your new project is available at /tmp/test
```

Enfin exécutez la commande `pelican`, le terminal vous répondra qu'aucun article n'a été généré (c'est normal) avec les lignes suivantes :

```
WARNING: No valid files found in content.
Done: Processed 0 articles, 0 drafts, 0 pages and 0 hidden pages in 0.05 seconds.
```

Vous devriez voir cependant qu'un dossier output est rempli de fichiers. Ouvrez le fichier `index.html` avec votre navigateur favori et Tadddaaa, votre premier site web statique sous vos yeux !

![premier site](https://zestedesavoir.com/media/galleries/5008/4b7c61b2-483c-40e8-8fdb-b1171ca13699.png)

Bon ok, c'est moche et inutile. Remédions à cela en attaquant le chapitre suivant qui va vous permettre de faire VOTRE site ;) .



