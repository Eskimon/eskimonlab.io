Title: Passer son serveur à l'httpS
Authors: Eskimon
Date: 2019-07-08 10:15
Modified: 2019-07-08 10:15
Slug: utiliser-un-certificat-ssl-lets-encrypt
Tags: web,tuto,infra
Licence: CC BY
Category: Web
Description: Voyons comment installer un certificat let's encrypt pour protéger notre trafic http


Dans ce petit tutoriel, nous allons voir comment installer un certificat SSL gratuit fourni par Let's Encrypt pour protéger le trafic http de notre serveur.
À la fin de ce tutoriel, tout le trafic http sera automatiquement forcé vers du http**S**.

# Sommaire

# Pré-requis

Pour réaliser ce tutoriel, je vais me baser sur un serveur utilisant l'OS **Ubuntu Server 18.04**. Le serveur web utilisé sera nginx et proposer une configuration minimal pour un site Django de la forme suivante :

```console
server {
    listen 80;
    server_name eskimon.fr;

    location / {
        proxy_pass http://localhost:8000/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
```

Le domaine visé par le certificat sera donc **eskimon.fr** et nginx se contente juste de rediriger le trafic arrivant sur le port 80 vers un service fonctionnant sur localhost au port 8000. Je pars du principe que ce fichier de configuration est sauvegardé selon les recommandations de nginx dans le fichier `/etc/nginx/sites-available/eskimon.fr`.

Voilà pour les présentations !

# Installation de certbot

Pour installer le certificat, nous allons commencer par installer un petit outil fourni par Let's Encrypt et qui se nomme `certbot`.

```console
sudo wget https://dl.eff.org/certbot-auto -O /usr/sbin/certbot-auto
sudo chmod a+x /usr/sbin/certbot-auto
```

À la première ligne, on récupère le logiciel sur le site de l'EFF en le sauvegardant dans le dossier `/usr/sbin/`. Ensuite, on donne la possibilité d'execution au logiciel. Et c'est tout !

*[EFF]: Electronic Frontier Foundation

# Création du certificat

Maintenant que tout est en place, il ne reste plus qu'à génerer le fameux certificat ! Pour cela, rien de plus simple, une seule commande à lancer :

```console
sudo certbot-auto certonly --nginx -d eskimon.fr
```

[[attention]]
| Il est possible que certaines dépendances systèmes soient manquantes, dans ce cas certbot les installera sans rien vous demander.

Durant l'installation, certbot vous demander un email (pour recevoir des alertes si vous oubliez de renouveler le certificat (ce qui n'arrivera pas d'après la section suivante)) ainsi que d'accepter les conditions d'utilisations de Let's Encrypt. Enfin, il demandera aussi si vous souhaitez partager votre email avec l'EFF pour recevoir des informations de leur part.

Une fois fini, la procédure devrait afficher un message similaire à celui-ci :

```console
Obtaining a new certificate
Performing the following challenges:
http-01 challenge for eskimon.fr
Waiting for verification...
Cleaning up challenges

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/eskimon.fr/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/eskimon.fr/privkey.pem
   Your cert will expire on 2019-10-06. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot-auto
   again. To non-interactively renew *all* of your certificates, run
   "certbot-auto renew"
 - Your account credentials have been saved in your Certbot
   configuration directory at /etc/letsencrypt. You should make a
   secure backup of this folder now. This configuration directory will
   also contain certificates and private keys obtained by Certbot so
   making regular backups of this folder is ideal.
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le
```

On apprend ainsi que les données du certificat sont dans le dossier `/etc/letsencrypt/live/eskimon.fr/`. Allons voir !

```
$ sudo ls -la /etc/letsencrypt/live/eskimon.fr/
total 12
drwxr-xr-x 2 root root 4096 Jul  8 10:32 .
drwx------ 3 root root 4096 Jul  8 10:32 ..
-rw-r--r-- 1 root root  692 Jul  8 10:32 README
lrwxrwxrwx 1 root root   39 Jul  8 10:32 cert.pem -> ../../archive/eskimon.fr/cert1.pem
lrwxrwxrwx 1 root root   40 Jul  8 10:32 chain.pem -> ../../archive/eskimon.fr/chain1.pem
lrwxrwxrwx 1 root root   44 Jul  8 10:32 fullchain.pem -> ../../archive/eskimon.fr/fullchain1.pem
lrwxrwxrwx 1 root root   42 Jul  8 10:32 privkey.pem -> ../../archive/eskimon.fr/privkey1.pem
```

# Mise à jour de la configuration

Maintenant que le certificat est obtenu, il ne reste plus qu'à mettre à jour la configuration nginx pour le prendre en compte.

Pour cela, allez éditer le fichier `/etc/nginx/sites-available/eskimon.fr` et faites en sorte d'avoir une configuration similaire à la suivante :

```console
server {
    listen 80;
    server_name	eskimon.fr;
    return 301 https://$server_name$request_uri;
}

server {
    listen 443 ssl http2;
    server_name	eskimon.fr;
    ssl_certificate /etc/letsencrypt/live/eskimon.fr/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/eskimon.fr/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/eskimon.fr/fullchain.pem;

    location / {
        proxy_pass http://localhost:8000/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
```

Concretement, il s'agit de rajouter un bloc `server` qui va gérer la partie SSL et de modifier le bloc concernant la partie http standard pour forcer la redirection du trafic vers le bloc http**S**.

Il ne reste plus qu'à recharger nginx pour constater les changements `sudo service nginx reload`.

# Renouvellement automatique du certificat

Maintenant que le certificat est en place, il ne nous reste plus qu'à activer une tâche de renouvellement automatique du certificat.

En effet, les certificats Let's Encrypt ne sont valides que 90 jours. Il faut donc les renouveler. Plutôt que de le faire à la main, nous allons activer une tâche CRON pour faire une vérification automatique de la validité et du renouvellement.

```
sudo crontab -e
# Ajouter la ligne suivant à la fin du fichier
0 2 * * * sudo /usr/sbin/certbot-auto -q renew
```

Et voilà, vos sites web peuvent maintenant recevoir du trafic chiffré en toute simplicité !
