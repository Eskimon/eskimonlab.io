Title: Hello World!
Authors: Eskimon
Date: 2018-10-27 09:00
Modified: 2018-10-27 09:00
Slug: tuto-pelican-201-hello-world
Tags: python,pelican,tuto
Licence: CC BY-NC-SA
Category: Web/B - Générons nos premières pages
Previous: tuto-pelican-102-pelican-comment
Next: tuto-pelican-202-les-métadonnées
Description: Le classique, première page Hello World générée avec Pelican.

Le "Hello World!", le grand classique des tutos d'informatique, pas de raison que l'on y échappe ici :D .

Nous allons voir ici comment bien démarrer un projet dans l'univers Pelican, rien de bien méchant mais c'est important de bien comprendre les bases !

Alors dépoussiérez vos claviers, on y va !

# Sommaire

# Quick Start

Si vous lisez ce tutoriel en continu, les dernières actions que nous avons faites pour créer un début de site sont encore fraîche dans votre mémoire. Pour les autres, remettons nous au jus.

## Rappel sur l'installation

Pour créer notre site de recettes de cookies (que j’appellerais *coolcookies*), nous allons créer un nouveau dossier de travail dans lequel nous allons tout de suite nous placer.

```bash
$ mkdir coolcookies
$ cd coolcookies
```

Maintenant, nous créeons un nouvel environnement virtuel que nous activons tout de suite.

```bash
$ virtualenv venv
$ source venv/bin/activate
```

Enfin, on installe dans cet environnement virtuel les différents paquets dont nous avons besoin (Pelican et markdown).

```bash
$ pip install pelican Markdown
```

## L'outil de démarrage rapide « *quickstart* »
****
Nous voilà avec notre base de travail. Nous allons maintenant utiliser l'outil « *quickstart* » de Pelican pour préparer le terrain. Cet outil interactif va vous poser quelques questions pour pré-remplir des fichiers de configurations. Pour le lancer, tapez simplement `pelican-quickstart`.

Voici un exemple de réponse possible à fournir, je vous explique juste après à quoi servent les différentes étapes.

```text
> Where do you want to create your new web site? [.] 
> What will be the title of this web site? Cool Cookies
> Who will be the author of this web site? Eskimon
> What will be the default language of this web site? [fr] 
> Do you want to specify a URL prefix? e.g., http://example.com   (Y/n) n
> Do you want to enable article pagination? (Y/n) 
> How many articles per page do you want? [10] 5
> What is your time zone? [Europe/Paris] 
> Do you want to generate a Fabfile/Makefile to automate generation and publishing? (Y/n) n
> Do you want an auto-reload & simpleHTTP script to assist with theme and site development? (Y/n) 
Done. Your new project is available at /home/eskimon/Documents/tuto-pelican
```

Voyons voir... (pour rappel, si les réponses par défaut (en majuscules) vous conviennent vous n'êtes pas obligés de spécifier quoi que ce soit, appuyez juste sur entrée).

1. Cette première ligne demande où doit être créer le projet. Étant déjà dans un dossier créé pour l'occasion, ce dernier me convient très bien, je laisse donc le choix par défaut (entre crochets).
2. Quel sera le titre de mon site. *Cool Cookies, pour des recettes de cookies qui sont cools*. (Vous remarquerez qu'aucun choix par défaut n'est proposé, l'outil ne lit pas encore dans les pensées).
3. Qui sera le créateur du site. Moi, Eskimon.
4. Quelle sera la langue par défaut des articles. `fr` pour Français.
5. Voulez-vous spécifier une URL de préfixe ? Si vous savez déjà quel domaine utilisera votre site alors spécifiez le. Sinon les URLS seront *relatives*, de la forme `/ma-super-recette` plutôt que `https://coolcookies.fr/ma-super-recette`. Cela ne perturbera pas le développement du site, pas d’inquiétude.
6. Souhaitez vous que les articles soient paginés ? Autrement dit "dans une page contenant une liste d'articles (page de catégorie par exemple), les articles doivent-ils tous être balancés dans une grande liste ou faut-il créer plusieurs pages listant qu'une partie des articles à chaque fois. On choisit de garder la pagination pour l'exemple.
7. Du coup, quelle sera le nombre d'articles maximum par page. 5, c'est bien pour illustrer le phénomène ;) .
8. Quelle est votre fuseau horaire ? Histoire que les articles soient horodatés correctement :)
9. Voulez-vous générer un fabfile. Si vous ne savez pas ce que c'est, ce n'est pas grave (c'est utilisé pour les déploiements automatisés notamment). Nous nous en passerons dans ce tutoriel, donc répondez **non**.
10. Enfin, voulez-vous un outil pour aider au développement, répondez oui ! (nous en reparlerons plus tard).
11. Bravo, votre squelette de site est prêt !

# Qui va là ?

## L'arborescence en détail


Si tout s'est bien passé, voici l'arborescence que vous devriez obtenir (en omettant le dossier de l'environnement virtuel) :

```plain
.
├── content
├── develop_server.sh
├── output
├── pelicanconf.py
└── publishconf.py
```

Comme on le voit dans cette arborescence, il y a plusieurs fichiers et deux dossiers. Voyons brièvement leur rôle.

1. :)
2. Le dossier `content`. En français "contenu", c'est ici que nous placerons nos rédactions au format markdown.
3. `develop_server.sh`. L'outil d'aide au développement, j'y reviendrais.
4. Le dossier `output`. C'est ici que va se retrouver notre site internet final mis en forme.
5. Le fichier de configuration de Pelican, pour régler certains paramètres permettant de passer du contenu au rendu final.
6. Idem que ci-dessus, mais pour reparamètrer certains aspects.

(Les points 5 et 6 seront vu en détail dans une des [parties annexes sur les paramètres de configuration](https://zestedesavoir.com/contenus/2497/creer-un-site-web-statique-avec-pelican/aller-plus-loin/quelques-parametres-de-configuration/). En effet, ces derniers sont déjà normalement plutôt bien réglés grâce à l'outil quickstart que nous venons d'utiliser.

Mais trêve de blabla, Créons notre premier article !

# Premiers articles

## Notre première recette

Sans perdre plus de temps, allez tout de suite créer un fichier dans le dossier `content`. Nommez le `cookies-pepites-chocolat.md`.

[[i]]
| Attention à l'extension du fichier. Ici j'ai bien marqué `.md`, qui fera comprendre à Pelican que ce fichier est rédigé en markdown.

Dans ce fichier, collez le contenu suivant.

```markdown
Title: Cookies aux pépites de chocolat
Date: 2018-05-04 12:42
Tags: cookie, chocolat
Category: Cookie
Authors: Eskimon
Summary: Ma recette pour des supers cookies aux pépites de chocolat.

Une première recette pour débuter dans l'art du cookie.

## Ingrédients

*(Pour 10 cookies)*

- 100g de pepites de chocolat
- 1 cuiller à café de levure
- 1/2 c à café de sel
- 150g de farine
- 1 sachet de sucre vanillé
- 1 œuf
- 85g de sucre (peut descendre à 50g)
- 75g de beurre (peut descendre à 50g)

## Étape 1 : Mélange

Ramollir le beurre au micro-ondes (sans le faire fondre).

Mélanger beurre, œuf, sucre et sucre vanillé. Ajouter la farine, le sel et la levure petit à petit, puis les pépites de chocolat.

## Étape 2 : Cuisson

Faites de petites boules, les mettre sur du papier sulfurisé.

Enfournez à 180°C pendant 10 à 12 min (suivant la texture que vous désirez).

## Étape 3 : Dégustez !

:D
```

Vous pouvez remarquer que le fichier est divisé en deux parties. Tout d'abord, une liste de données du type "Information: Valeur". C'est ce que l'on appelle des *metadata*, métadonnées en français, et nous verrons leur rôle dès le chapitre suivant. Suite à cela, on trouve le corps de notre article, rédigé au format markdown.

Maintenant, à la racine de votre projet (donc pas dans le dossier content ou output), lancer la commande `pelican` (tout simplement). Si tout se passe normalement, vous devriez avoir la réponse suivante : 

```bash
$ pelican
Done: Processed 1 article, 0 drafts, 0 pages and 0 hidden pages in 0.11 seconds.
```

Vous êtes alors informé qu'un article a bien été traité. Voyons voir ce qui se passe dans notre arborescence dorénavant...

```plain
.
├── content
│   └── cookies-pepites-chocolat.md
├── develop_server.sh
├── output
│   ├── archives.html
│   ├── author
│   │   └── eskimon.html
│   ├── authors.html
│   ├── categories.html
│   ├── category
│   │   └── cookie.html
│   ├── cookies-aux-pepites-de-chocolat.html
│   ├── index.html
│   ├── tag
│   │   ├── chocolat.html
│   │   └── cookie.html
│   ├── tags.html
│   └── theme
│       ├── [...]
├── pelicanconf.py
└── publishconf.py
```

Vous remarquez que le dossier `output` a maintenant tout un tas de dossiers et de fichiers ? Ceux qui attirent l'attention sont notamment :

- `authors`, un dossier qui propose une page HTML par auteur identifié. Il est accompagné d'un fichier `authors.html`qui va lister tous les auteurs sur le site ;
- `category` et `categories.html`, similaires à auteurs mais pour les categories de contenu sur votre site ;
- `tag` et `tags.html`, vous avez deviné ? ;) ;
- Un dossier `theme` qui contiendra tous les fichiers *statiques* (feuille de style CSS, images, script, etc). Je vous le présenterait en détail dans la partie sur la création de votre propre thème ;
- Enfin, deux fichiers HTML sont à la racine de ce dossier output. `index.html` et `cookies-aux-pepites-de-chocolat.html`. Le fichier index sera l’accueil de votre site, la page qui sera présentée quand un visiteur arrive directement sur la racine du site. La page `cookies-aux-pepites-de-chocolat.html` quant à elle sera la page contenant votre fameuse recette !

Vous pouvez déjà essayé d'aller voir votre travail en ouvrant ces deux pages avec votre navigateur préféré.

Vous devriez alors voir votre recette (oui je sais, c'est moche pour le moment) !

[[q]]
| Mais les deux pages sont identiques ?

Effectivement, pour l'instant nous n'avons qu'une seule recette et le rendu par défaut fait que la page index a la même allure que la page de la recette.  
Ajoutons une deuxième recette pour voir la différence en créant le fichier suivant dans `content/cookies-bananes.md`

```markdown
Title: Cookies à la banane
Date: 2018-05-04 13:42
Tags: cookie, banane
Category: Cookie
Authors: Eskimon
Summary: Recette de cookies à la banane

Partons à l'aventure en faisant des cookies avec de la banane et du chocolat

## Ingrédients

*(Pour une vingtaine de cookies)*

- 170 g de bananes (à peser sans la peau ;) )
- 150 g de farine
- 60 g de beurre
- 5 g de levure chimique
- 1 pincée de sel
- 1 oeuf
- 100 g de sucre en poudre
- 1 cuillère à café de vanille liquide
- 130 g de chocolat

## Étape 1

Préchauffez le four à 190°C (et pas plus!!).

## Étape 2

Mélangez la farine, la levure, le sel.

## Étape 3

Pelez les bananes et écrasez-les à la fourchettes.

## Étape 4

Battez le beurre (bien ramolli) avec le sucre ; ajoutez la banane écrasée, l'oeuf, la vanille. Mélangez bien. Ajoutez le mélange de farine.

## Étape 5

Ajoutez les pépites (+ noix). La pâte doit être molle mais pas trop liquide.

## Étape 6

Faites des petits tas à l'aide de 2 cuillères à café (de la taille d'une noix, pas plus!!) sur une plaque (soit beurrée soit recouverte de papier sulfurisé, + facile!).

## Étape 7

Faites cuire env 6/7 mn (selon votre four).

## Étape 8

Pour la 1ère fournée, surveillez bien, ça cuit très vite. Retirez les cookies dès qu'ils sont bruns à la base.
```

Régénérez le site avec la commande `pelican` et observez de nouveau le dossier output. Un nouvel article a du faire son apparition.

Si vous ouvrez de nouveau la page `index.html`, cette dernière devrait présenter les deux recettes à la suite ;) .

## Ajouter des images

Le texte c'est bien, mais avec des illustrations, c'est mieux !

Pour ajouter des illustrations, il va falloir faire plusieurs choses. Tout d'abord, nous allons créer un dossier `images` dans le dossier `content` où nous avons écrit nos articles jusqu'à présent.

Ensuite, il faut informer Pelican que ce dossier est un dossier "statique" et qu'il faut copier le contenu tel quel dans le dossier de sortie "output". Pour cela, il faut ajouter une ligne de configuration dans le fichier `pelicanconf.py`. Cette ligne sera `STATIC_PATHS = ['images']`. Si jamais vous avez d'autres dossiers à exporter (comme un dossier contenant des exports pdf des articles par exemple), il suffit des les rajouter dans la liste.

Enfin, il ne reste plus qu'à utiliser la bonne syntaxe pour insérer l'image. Par exemple, en markdown on écrira `![Texte alternatif de ma belle image](/images/ma-belle-image.png)`.

Et voilà, vous devriez maintenant être familier avec l'arborescence du projet que nous allons manipuler ainsi que les fichiers de base le composant. Nous allons maintenant voir comment enrichir nos articles en ajoutant des informations contextuels via les *metadata*.

[[i]]
| Et sinon, avez-vous eu la curiosité d'aller voir les pages supplémentaires dans les dossiers tags ou categories ? ;)

