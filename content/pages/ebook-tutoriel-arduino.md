Title: Ebook du tuto Arduino
Slug: ebook-tutoriel-arduino
Save_as: ebook-tutoriel-arduino.html
Tags: ebook,tutoriel,arduino
open_comments: True
Description: Vous l'avez voulu, et bien le voilà, le tutoriel Arduino à emporter partout pour le consulter dès que vous avez besoin ! Version epub ou pdf, au choix !


# Les bouquins

Il est beau il est chaud, le livre version numérique du tutoriel !
Vous pourrez maintenant emporter partout avec vous le tutoriel pour le consulter dès que vous avez besoin.
Dans le bus, dans le train, voici un tuto tout terrain !

Pas de partie supprimée, aucune surprise, cette version à transporter est presque la même que sa consoeur la version en ligne.

On y retrouve donc les 8 parties, une trentaine de chapitres pour un total de 630 pages. Seuls sont absents les chapitres de la partie Annexes sur le site.

Deux versions vous sont proposées : pdf format A4 et ebook format epub3 pour les liseuses et autres tablettes.

Voici les liens :

- [Le pdf](/extra/ebooks/arduino-premiers-pas-en-informatique-embarquee.pdf)
- [L'ebook](/extra/ebooks/arduino-premiers-pas-en-informatique-embarquee.epub)

# Informations

<div class="custom-block alert alert-primary">
 <div class="custom-block-body">
  <p>
   La mise à disposition de cette ressource numérique est gratuite.
  </p>
 </div>
</div>

Elle reflète le fruit de travail d’écriture et de recherches de plusieurs années, de temps pour faire des vidéos avec leurs montages et de nombreux schémas.
Je ne ferais pas payer pour télécharger, ni vous pourrirais la vie en vous obligeant à regarder une publicité.
C’est gratuit pour tout le monde et ça le restera.

<div class="custom-block alert alert-primary">
 <div class="custom-block-body">
  <p>
   Si vous souhaitez remercier pour tout le travail accompli et qui continuera à paraître, je vous invite à faire un don du montant qui vous plaira ci-dessous.
  </p>
 </div>
</div>

(Une autre conséquence de la mise à disposition d’un ebook est que le trafic risque de diminuer et le nombre d’affichages de pub sur le blog aussi).

- Faire un don en Ğ1 : **FyRYQUg3UyPh532mzZW2xidLW47XPwEWMteroVdzprPo**
- Faire un don via paypal !
<form style="display:inline" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input name="cmd" value="_s-xclick" type="hidden">
<input name="encrypted" value="-----BEGIN PKCS7-----MIIHLwYJKoZIhvcNAQcEoIIHIDCCBxwCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYBoTnRyPMY694CelGOsR+2pqyqoYSijCygTgwzX8PYF3Bl8emDr1ABkdXL0vWx1CDJp6436y3FDwSoAyCsV3x+4Atqy7+vKOmek1qjW3hCMV98gmvRe/dG3ukKIix38QMWcPrUn3CJSglomHIubUGIbqZG+nOKdyVaAI3SLr9MKcjELMAkGBSsOAwIaBQAwgawGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQIzH1MfpUXMUaAgYjUu+2EePFSa0n90k4vvZVVEly+mLHP2I7VkaqrjC54YP8p57YqjjMIduGT52Cdpf5vLwXj1uMFHFnutx3xHp3KfcSpRJ9GtkqIPkuBXTga0W8ZZdRkfJ0zxcVrCnMla1B/Z/DYbdNWFTvZOmZtWddkBbMEf0d8qbiWL8Om2kuOhPSSUehm8uyGoIIDhzCCA4MwggLsoAMCAQICAQAwDQYJKoZIhvcNAQEFBQAwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMB4XDTA0MDIxMzEwMTMxNVoXDTM1MDIxMzEwMTMxNVowgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBR07d/ETMS1ycjtkpkvjXZe9k+6CieLuLsPumsJ7QC1odNz3sJiCbs2wC0nLE0uLGaEtXynIgRqIddYCHx88pb5HTXv4SZeuv0Rqq4+axW9PLAAATU8w04qqjaSXgbGLP3NmohqM6bV9kZZwZLR/klDaQGo1u9uDb9lr4Yn+rBQIDAQABo4HuMIHrMB0GA1UdDgQWBBSWn3y7xm8XvVk/UtcKG+wQ1mSUazCBuwYDVR0jBIGzMIGwgBSWn3y7xm8XvVk/UtcKG+wQ1mSUa6GBlKSBkTCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb22CAQAwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOBgQCBXzpWmoBa5e9fo6ujionW1hUhPkOBakTr3YCDjbYfvJEiv/2P+IobhOGJr85+XHhN0v4gUkEDI8r2/rNk1m0GA8HKddvTjyGw/XqXa+LSTlDYkqI8OwR8GEYj4efEtcRpRYBxV8KxAW93YDWzFGvruKnnLbDAF6VR5w/cCMn5hzGCAZowggGWAgEBMIGUMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbQIBADAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTQwMzEzMjIzMTQ5WjAjBgkqhkiG9w0BCQQxFgQUsNC66IeGyRPeszIbIuD24W1UwDIwDQYJKoZIhvcNAQEBBQAEgYAAzrfw/Sa3AtqDSu7U7V47Yk3Y/R/MoP/yS2H8e14/h5hm7c7lhXhzECN5tQ9OLhf30mbiAesO6xuMKoqCuhdX7isjJpPNfti83R1OshXWVD+2VQeBkJSHmayJSbWfJe4w9J4HDsxj9vog3eS8m8JKagH69F8vizMpjW0N+94hcw==-----END PKCS7-----" type="hidden">
<input src="https://www.paypalobjects.com/fr_FR/FR/i/btn/btn_donate_LG.gif" name="submit" title="Faire un cadeau à Eskimon :)" alt="Faire un cadeau à Eskimon :)" type="image" border="0">
</form>
