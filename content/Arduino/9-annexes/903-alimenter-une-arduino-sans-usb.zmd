Title: Alimenter une Arduino sans USB
Authors: Eskimon
Date: 2014-03-22 09:00
Modified: 2014-03-22 09:00
Slug: tuto-arduino-903-alimenter-une-arduino-sans-usb
Tags: arduino,tuto
Licence: CC BY
Category: Arduino/I - Annexes
Previous: tuto-arduino-902-gestion-de-la-mémoire-sur-arduino
Next: tuto-arduino-904-les-ports
Description: Je vous propose dans ce mini-tutoriel de vous passer de l'USB pour alimenter votre Arduino. Vous allez voir, plusieurs solutions simples existent.
permalink_id: 1626-arduino-mini-tuto-alimenter-son-arduino-sans-usb
Canonical: https://zestedesavoir.com/tutoriels/612/alimenter-une-arduino-sans-usb

Vous vous êtes peut-être déjà demandé comment faire pour devenir indépendant de votre ordinateur en terme d'alimentation afin de pouvoir utiliser votre super programme partout ? Eh bien voici quelques solutions qui peuvent être appliquées !

# Sommaire

# Principe et pré-requis

La carte Arduino possède un **régulateur** de tension pour fournir le 5V nécessaire au microcontrôleur lorsque celui-ci est alimenté via une source externe. Ce dernier est capable de transformer une source de tension continue "élevée" (dans notre cas supérieur à 5V) en tension plus faible et **régulée** à 5V. Attention, tout n'est cependant pas possible, il y a des limites à respecter dont nous allons parler plus tard pour ne pas tout griller.

Le régulateur est capable de fournir au maximum 1 ampère en sortie. Souvent, lorsque l'on début ou que l'on prototype, on se contente de faire des montages et de les laisser connecté à l'ordinateur. La carte est donc directement alimentée via l'USB en 5V (limité au meilleur des cas à un maximum de 500mA). Mais s'en est assez ! Nous voulons plus d'indépendance pour pouvoir emmener nos montages partout ! Il existe pour cela plusieurs solutions que l'on peut ranger en deux catégories : Celles qui nous fournissent du 5V directement et celles qui nous fournissent une tension plus élevée...

## Alimenter la carte Arduino avec une source de tension 5V

Les sources de 5V sont monnaie courante de nos jours. On en retrouve par exemple énormément avec tous les appareils mobiles qui demande une alimentation USB pour être rechargé (chargeur de portable, "power pack" pour faire une batterie d'appoint...). Une fois que vous possédez cette dernière, il ne reste plus qu'à la relier à l'Arduino. Pour cela, on va passer au-delà du régulateur et amener directement la tension sur la borne "+5V" (et bien sûr sur le GND). Il est aussi possible d'adapter un câble USB pour amener cette tension sur le connecteur USB (et ainsi limiter les erreurs possibles de branchement). C'est la solution la plus simple.

## S'alimenter avec autre chose

Si nous n'avons pas à disposition une source de tension 5V, il va falloir passer par le régulateur intégré à la carte pour l'obtenir. Comme le précise la documentation officielle, une tension de 7 à 12V est recommandée et les limites acceptées sont entre 6 et 20V. À 6V la carte risque de mal fonctionner (reset intempestif) et à 20V le régulateur va beaucoup chauffer et risque de mourir prématurément.

Pour amener notre tension externe, nous avons deux solutions :

+ La première est d'utiliser la fiche ronde et noire, qui se situe près de l'entrée USB. Cette fiche, que l'on appelle un **connecteur jack** femelle, fait 2.1mm de diamètre et possède deux connecteurs. L'un est une sorte de "pointe" qui est au milieu. Ce sera le pôle positif. L'autre est plaqué contre le bord intérieur du connecteur et fera connexion avec la masse.

Ce connecteur se dessine par le symbole électronique suivant :

![Symbole du centre positif](https://zestedesavoir.com/media/galleries/1126/acad0118-35b4-4d76-b474-2b6e8093860d.png.960x960_q85.png)

+ L'autre solution consiste à amener directement nos fils sur les broches Vin et Gnd. Je ne recommande cependant pas cette méthode qui augmente les risques d'erreurs de manipulation et donc de court-circuit (qui se résulteront par la mort du régulateur voire plus).

Maintenant que les présentations sont faites, il ne reste plus qu'à relier une source d'énergie et l'Arduino ensemble :) .

# Solutions

## Avec une pile 9V

Lorsqu'on réfléchit à retirer la laisse USB de notre carte Arduino, l'une des premières idées est surement d'utiliser une pile 9V. En effet, cette dernière comporte plusieurs avantages :

+ On en trouve facilement
+ Encombrante mais pas trop
+ Coût raisonnable
+ Sa tension est pile dans l'intervalle recommandé de [7V - 12V]

Maintenant, le plus dur reste à faire : relier la pile et la carte Arduino. Problème, la pile possède deux plots bizarroïdes et l'Arduino a le fameux jack femelle. Pour pouvoir les relier il va donc falloir bricoler un peu. Du côté de la pile, on utilisera le connecteur suivant (récupérable sur pas mal de vieux jouets) :

![Un support de pile 9V](https://zestedesavoir.com/media/galleries/1126/8d665ddc-a8a7-47e0-98dd-4e5d54630fd4.png.960x960_q85.jpg)

Du coté Arduino, nous l'avons vu plus tôt, il faudra un connecteur jack mâle. En les reliant ensemble (avec un domino, de la soudure, breadboard...) tout en respectant les couleurs vous aurez enfin la joie de pouvoir libérer votre Arduino du joug terrible de l'ordinateur et son câble USB :P !

Il existe aussi des versions toutes faites de ce câble chez certains [web]marchands sous la forme suivante :

![Support de pile relié au jack d'alimentation Arduino](https://zestedesavoir.com/media/galleries/1126/f643405b-e145-42e0-a9f8-e257b278ab4f.jpg.960x960_q85.jpg)

## Avec plusieurs piles 1.5V

Comme nous l'avons vue un peu plus tôt, le régulateur 5V de l'Arduino exige au grand minimum 6V pour fonctionner. En utilisant des piles rondes de type AAA ou AA, on pourrait en mettre plusieurs en série (4 par exemple) pour obtenir les fameux 6V. Génial non ? Eh bien pas tant que ça. En effet, il y a un souci. Le 6V est le minimum vital pour le régulateur. Autrement, dit si la tension fluctue un peu il risque de ne pas pouvoir faire son travail correctement et le microcontrôleur va faire des resets n'importe quand. De plus, lorsque les piles sont neuves cela peut aller, mais dès qu'elles vont être un peu utilisées, la tension à leurs bornes ne sera plus réellement 1.5V mais légèrement inférieure, ce qui risque d'augmenter le problème mentionné ci-dessus. Une solution : utiliser une cinquième pile pour porter le total à 7.5V. Ainsi, même quand les piles commencent à se décharger il reste un peu de marge pour que le régulateur fasse correctement son travail. Si vous voulez jouer la prudence, vous pouvez carrément en rajouter une sixième ! :P Comme pour la pile 9V, il va falloir bricoler pour pouvoir relier un support de pile de ce type au jack rond de l'Arduino (encore une fois : attention aux polarités !) :

![Support de pile AA](https://zestedesavoir.com/media/galleries/1126/a539e632-cca4-4e84-ba1f-ca55eea5ae8f.png.960x960_q85.png)

## Avec une alimentation (presque) quelconque

Il y a quelques temps, j'avais un disque dur externe. Malheureusement, une mésaventure se terminant par une chute en fonctionnement lui a fait rendre l'âme :( . Je me suis donc retrouvé avec un câble USB type B (bien utile avec l'Arduino) et une alimentation inutile. Inutile ? pas tant que ça ! Il s'avère (coup de chance) que l’extrémité du câble de l'alimentation rentre parfaitement dans la prise femelle du jack d'alimentation de l'Arduino. Un rapide coup d'oeil sur le bloc d'alimentation secteur me donne les caractéristiques qui m'intéressent :

+ Entrée : 100 - 240V alternatif 50-60 Hz (plutôt standard)
+ Sortie : **12 V**, 1.5 A continu. BINGO !

Me voilà avec une alimentation prête pour une nouvelle vie ! Un rapide coup d’œil sur le boitier pour m'assurer que le positif est bien au centre du jack (symbole ci-dessous) et c'est terminé !

![Symbole du centre positif](https://zestedesavoir.com/media/galleries/1126/acad0118-35b4-4d76-b474-2b6e8093860d.png.960x960_q85.png)

![Le chargeur recyclé](https://zestedesavoir.com/media/galleries/1126/e1fdf7e8-0643-4620-aa29-81d0583cc1bf.png.960x960_q85.png)

[[i]]
| Ce chargeur, capable de débiter 1.5A sous 12V, serait même capable de fournir assez d'énergie pour alimenter un petit moteur via un shield moteur en reprenant directement les 12V sur la broche Vin par exemple[^limite].

[^limite]: Attention, la broche est tout de même limitée à un ampère nominal car protégée par une diode pouvant laisser passer 1A dans des conditions normales de fonctionnement.

## D'autres solutions ?

Les idées proposées ci-dessus ne sont pas les seuls. Vous en avez peut-être d'autres qui pourraient-être utilisées. En fait, n'importe quelle source d'énergie est viable, rappelez-vous juste qu'elle doit être en mesure de fournir plus de 7V continu et dans la mesure du possible moins de 12V (mais si ça fait 15V ce n'est pas dramatique non plus, le régulateur chauffera juste un peu plus). Parmi quelques idées en vrac on pourrait trouver :

+ Une batterie de voiture (environ 12V)
+ La prise allume-cigare de la voiture (11 à 13V selon que la voiture est à l'arrêt ou en marche)
+ Des panneaux solaires ?
+ Une dynamo ? (avec un pont de diode et un condensateur pour filtrer)
+ Soyez créatif ! (et n'hésitez pas à venir en parler en commentaire pour faire part de vos idées et expériences :) )



