Title: A petits pas, le moteur pas-à-pas
Authors: Eskimon
Date: 2013-12-12 12:22
Modified: 2013-12-12 12:22
Slug: tuto-arduino-603-a-petits-pas-le-moteur-pas-à-pas
Tags: arduino,tuto
Licence: CC BY-NC-SA
Category: Arduino/F - Le mouvement grâce aux moteurs
Previous: tuto-arduino-602-un-moteur-qui-a-de-la-tête-le-servomoteur
Next: tuto-arduino-701-les-écrans-lcd
permalink_id: 290-arduino-603-petits-pas-le-moteur-pas-pas
cover-pic: pic.png
Description: Cette partie traite du fonctionnement des différents moteurs pas-à-pas et comment les interfacer avec une carte Arduino.

Pour en terminer avec les différents types de moteurs qui existent, nous allons parler d'un moteur un peu particulier (encore plus que le servomoteur !) et qui est cependant très utilisé dans le domaine de la robotique et tout ce qui touche à la précision d'un mouvement. Comme à l'habitude, nous allons d'abord voir le fonctionnement de ces moteurs, pour ensuite apprendre à les utiliser.

[[i]]
| Ce moteur utilise des éléments que nous avons vus dans des chapitres précédents (sur les moteurs à courant continu). Si vous ne vous rappelez pas du L298 je vous conseille de retourner prendre quelques informations à ce sujet. ;)

# Sommaire

# Les différents moteurs pas-à-pas et leur fonctionnement

Les moteurs pas-à-pas... encore un nouveau type de moteur. Une question vous taraude sûrement l'esprit :

[[q]]
| Pourquoi il existe tant de moteurs différents !?

Et bien je vous répondrais par une autre question : pourquoi existe-t'il autant de langages de programmation différents !? La réponse est pourtant simple : car ils ont tous leurs avantages et leurs inconvénients. Par exemple, un servomoteur pourra facilement maintenir la position de son axe, tandis que le moteur à courant continu sera plus facile à faire tourner à différentes vitesses. Eh bien, le but du moteur pas-à-pas (que j'abrégerais moteur pàp) est un peu une réunion de ces deux avantages. Vous pourrez le faire tourner à des vitesses variables et la position parcourue sera aussi facile à déterminer. En contrepartie, ces moteurs ne peuvent pas tourner à des vitesses hallucinantes et sont plus délicats à mettre en œuvre que les moteurs CC par exemple (mais rien d'insurmontable je vous rassure).

En parlant de précision, savez-vous dans quel objet du quotidien on retrouve beaucoup de moteurs pàp ? Dans l'imprimante (éventuellement scanner aussi) qui traîne sur votre bureau ! En effet, l'aspect "précision" du moteur est utilisé dans cette situation sans avoir besoin d'aller vraiment vite. Vous pourrez donc en trouver un pour faire avancer les feuilles et un autre pour déplacer le chariot avec les cartouches d'encre (et encore un autre pour déplacer le capteur du scanner). Donc si vous avez une vieille imprimante destinée à la poubelle, vous savez ce qu'il vous reste à faire ;) ! Les moteurs que vous pourrez trouver posséderont 4, 5 voire 6 fils. Le premier (4 fils) est appelé **moteur bipolaire**, les deux autres sont des moteurs **unipolaires** ou à **réluctance variable**. Tout cela doit-être encore un peu confus. Voyons donc plus clairement comment cela marche !

## Fonctionnement des moteurs

Comme précédemment avancé, ce moteur possède une certaine complexité pour être mis en œuvre. Et ce, plus que les précédents. Vous souvenez-vous du moteur CC (j’espère bien ! :P ). Il était composé d'un ensemble d'aimants sur l’extérieur (le stator) et d'une partie bobinée où le champ magnétique était créée dynamiquement avec un ensemble collecteur/balais qui transmettait l'électricité aux bobines au centre (rotor).

Dans le cas du moteur pàp, c'est sur le rotor (au centre) que l'on retrouve l'aimant permanent, et les bobines sont sur le stator (autour du rotor). Comme pour les moteurs à courant continu, le but du jeu, en quelque sorte, est de "faire tourner un champ magnétique" (à prendre avec des pincettes) pour faire tourner l'aimant fixé au rotor. Il existe cependant différents types de moteurs pàp dont le placement des bobinages diffère les uns des autres et la façon de les alimenter n'est pas non plus identique (d'où une complexité supplémentaire lorsque l'on veut changer le type de moteur pàp à utiliser...). Nous allons maintenant les étudier l'un après l'autre en commençant par celui qui semble avoir le fonctionnement le plus simple à assimiler.

### Moteur pàp bipolaire à aimants permanents

Ce moteur possède quatre fils d'alimentation pour piloter des bobines par paire. Comme un schéma vaut mieux qu'un long discours, voici comment il est constitué :

![Vue schématisée et simplifiée d'un moteur pas-à-pas bipolaire](https://zestedesavoir.com/media/galleries/954/605f9301-94fb-44e4-b45e-be6a09a3f42d.png.960x960_q85.png)

Vous l'aurez compris, les bobines sont reliées deux à deux en série et sont donc pilotées ensemble. Il n'y a donc finalement que deux enroulements à commander puisque deux bobines montées en série n'en font plus qu'une. Leur placement de part et d'autre de l'aimant permanent du rotor permet de piloter ce dernier. Voyons comment.

+ Lorsqu'il n'y a aucun courant traversant les bobines, le rotor (où l'axe de sortie est lié) est libre de tourner, rien ne cherche à le retenir dans sa course.
+ Maintenant, si nous décidons de faire passer du courant entre les points C et D pour alimenter la bobine de gauche et celle de droite. Un courant va s'établir et deux champs électromagnétiques vont apparaître de part et d'autre du rotor. Que va-t-il alors se passer ? L'aimant du rotor va tourner sur lui-même pour se placer de façon à ce que son pôle Nord soit en face du pôle Sud du champ magnétique créé dans la première bobine et que son pôle Sud soit en face du pôle Nord créé dans la deuxième bobine.
+ Si ensuite on alimente non plus les bobines entre C et D mais plutôt celles entre A et B, le rotor va alors tourner pour s'aligner à nouveau vers les pôles qui l'intéressent (Nord/Sud, Sud/Nord).
+ Et c'est reparti, on va alors alimenter de nouveau les bobines entre D et C, donc avec un courant de signe opposé à la fois où l'on les a alimenter entre C et D (par exemple C était relié au "+" de l'alimentation tout à l'heure et là on le fait passer au "-", idem pour D que l'on fait passer du "-" au "+") et le moteur va encore faire un quart de tour.
+ On peut continuer ainsi de suite pour faire tourner le moteur en faisant attention de ne pas se tromper dans les phases d'alimentation.

À chaque phase on va donc faire tourner le moteur d'un quart de tour :

![Rotation pas-à-pas du moteur](https://zestedesavoir.com/media/galleries/954/f3b3a2be-a158-4bb7-ad25-c23f6a672474.png.960x960_q85.png)

[[i]]
| Ce quart de rotation s'appelle un **pas**. Et comme il faut plusieurs pas pour faire tourner le moteur sur 360°, on l'a donc appelé ainsi, le moteur *pas-à-pas*.

Dans le cas illustré ci-dessus, on dit que le moteur fait 4 pas par tour. Il existe bien des moteurs qui font ce nombre de pas, mais il en existe qui ont un nombre de pas plus conséquent (24, 48, etc.). Leur constitution mécanique est différente, ce qui leur confère ce pouvoir, bien que le fonctionnement reste identique, puisque l'on cherche toujours à attirer un aimant grâce à des champs magnétiques crées par des bobines parcourues par un courant. Pour avoir plus de pas, on multiplie les aimants au centre. Sur l'image ci-dessous, on peut bien voir les bobines (en cuivre à l’extérieur) et tous les aimants au centre (les petites dents). Il existe aussi deux autres modes de fonctionnement que nous verrons dans la partie suivante : **le pilotage avec couple maximal** et **le pilotage par demi-pas**.

![Vue en coupe d'un moteur pas-à-pas bipolaire](https://zestedesavoir.com/media/galleries/954/8ec3ce2a-97a0-49bb-9045-1e0c08021547.jpg.960x960_q85.jpg)
Figure: Vue en coupe d'un moteur pas-à-pas bipolaire - (CC-BY-SA, [vincenzov.net](http://commons.wikimedia.org/wiki/File:Struttura_motore_passo-passo.jpg))

![Animation de la rotation d'un moteur pas à pas](http://zestedesavoir.com/media/galleries/954/729ab71c-6fc3-40bb-ba13-191c3c996cb3.gif)
Figure: Animation de la rotation d'un moteur pas à pas - (CC-BY-SA, [Teravolt](http://commons.wikimedia.org/wiki/File:StepperMotor.gif))

Pour rappel, voici la vue d'un moteur à courant continu :
[[s]]
| ![Vue interne d'un moteur CC](https://zestedesavoir.com/media/galleries/954/8c648145-d9b3-497f-88ed-85477c450b96.png.960x960_q85.jpg)

### Le moteur unipolaire

Le moment est enfin venu de vous révéler la véritable signification des noms du moteur vu précédemment et de celui-ci même... non il ne faut pas lire ça sur un ton tragique. ^^ Le moteur bipolaire est nommé ainsi car il présente la faculté d'être commandé en inversant simplement la polarité de ces bobinages. Quant au moteur unipolaire, pas besoin de faire cette inversion, chaque bobinage est commandé séparément et ne requiert qu'une alimentation présente ou absente selon que l'on veuille ou non créer un champ magnétique en son sein. La commande sera donc plus simple à mettre en place qu'avec le moteur bipolaire. Cependant, le nombre de bobines étant plus important, la quantité de cuivre également et le prix s'en ressent ! En effet, il s'agit bien de 4 bobines bien distinctes, alors que le moteur bipolaire à aimant permanent en possède finalement quatre moitiés de bobines, donc deux bobines complètes.

![Vue en coupe d'un moteur pas-à-pas unipolaire](https://zestedesavoir.com/media/galleries/954/ff0f59aa-8779-4a4e-a7cc-7284325532f4.png.960x960_q85.png)

On retrouve nos quatre fils A, B, C et D ainsi qu'un fil de masse commun (bon ben imaginez-le puisqu'il n'est pas dessiné comme tel sur le schéma). Soit 5 fils (v'là pas qu'on sait compter maintenant ! ^^ ). Le fonctionnement est rigoureusement identique que le précédent moteur. On cherche à créer un champ magnétique "rotatif" pour faire passer l'aimant alternativement devant chacune des bobines. On va donc alimenter la bobine A, puis la C, puis la B, puis la D selon le schéma ci-dessus. Et voilà, le moteur aura fait tout un tour assez simplement. Il suffit d'avoir quatre transistors (un par enroulement) sans avoir besoin de les disposer en H et de les piloter deux à deux. Ici il suffit de les alimenter un par un, chacun leur tour. Facile, non ? ;)

### Le moteur à réluctance variable

[[q]]
| Gné :o ? c'est quoi ce nom barbare ?

Ce moteur est un peu plus compliqué, mais c'est aussi le dernier que nous verrons et le plus fascinant ! Contrairement aux deux précédents, ce moteur ne possède pas d'aimants permanents, ni même d'aimant tout court ! Non, en son centre on trouve un simple morceau de **fer doux**. Ce dernier à la particularité de très bien conduire les champs magnétiques. Du coup, si un champ magnétique le traverse, il voudra absolument s'aligner dans son sens. C'est cette propriété qui est exploitée. Commençons par voir tout de suite le schéma de ce moteur :

![Vue en coupe d'un moteur pas-à-pas à réluctance variable](https://zestedesavoir.com/media/galleries/954/fd25b73a-3117-4cc1-8fa3-e96ad0842cfa.png.960x960_q85.png)

Comme vous pouvez le voir, il possède 4 enroulements (formant 4 paires) et le morceau de fer doux au milieu à une forme d'étoile à 6 branches. Et ce n'est pas un hasard ! Le ratio de 6 pour 8 a une raison très précise car cela introduit un léger décalage (15°) entre une branche et une bobine. En effet, si l'on a 8 bobines (4 paires) on a un décalage entre chaque bobine de : $\frac{360^\circ}{8}=45^\circ$ Donc tous les 45° le long du cylindre qu'est le moteur, on trouve un bobinage. En revanche il n'y a que 60° entre chaque extrémité de l'étoile du rotor : $\frac{360^\circ}{6}=60^\circ$ Mais pourquoi exactement ? Eh bien, c'est simple avec un peu d'imagination (et quelques dessins). Si l'on commence par alimenter le premier enroulement, le A, le rotor va s'aligner avec. Maintenant que se passera-t-il si l'on alimente B ? Le rotor, qui était alors positionné avec une de ses branches bien en face de A, va bouger pour s'aligner correctement vers B. Ensuite, si l'on alimente C il va se passer de même, le rotor va encore tourner de 15° pour s'aligner, etc. Si l'on effectue cette opération 24 fois, on fera un tour complet car : $24 \times 15^\circ = 360^\circ$

[[i]]
| Vous remarquerez que dans cet exemple le rotor tourne dans le sens horaire alors que l'alimentation des bobines se fera dans le sens antihoraire.

Ces moteurs ont certains avantages. Parmi ces derniers, il n'y a pas besoin de polariser les bobines (peu importe le sens du champ magnétique, l'entrefer n'étant pas polarisé essaiera de s'aligner sans chipoter). Le fait que l'on utilise un simple entrefer en fer doux le rend aussi moins cher qu'un modèle avec des aimants permanents. Vous savez maintenant tout sur les trois types de moteurs pas-à-pas que l'on peut trouver, place maintenant à la pratique !

[[a]]
| De manière générale, n'essayez pas d'ouvrir vos moteurs pas-à-pas pour regarder comment c'est fait et espérer les remonter après. Le simple démontage à tendance à faire diminuer la qualité des aimants permanents à l'intérieur et donc votre moteur ne sera plus aussi bon après remontage.

# Se servir du moteur

Continuons à parler de notre super moteur. Si vous avez suivi ce que j'ai dit plus tôt, j'ai expliqué qu'il y avait des bobines qui généraient un champ magnétique. Lorsqu'elles sont alimentées, ces bobines ont besoin de courant pour pouvoir générer un champ magnétique suffisant. Vous ne serez donc pas surpris si je vous dis qu'il faudra utiliser un composant pour faire passer la puissance dans ces dernières. Et là, comme les choses sont bien faites, nous allons retrouver le pont en H et le L298. :)

[[i]]
| Afin de limiter la redondance d'informations, je vais me contenter de vous expliquer le pilotage du moteur unipolaire et bipolaire. Si vous comprenez correctement ces derniers, vous n'aurez aucun problème avec le moteur restant ;)

[[e]]
| Les schémas qui vont suivre ont pour source d'énergie une pile +9V. Cependant il est déconseillé de les faire avec car la consommation des moteurs est assez importante et la pile risque de se fatiguer très vite. Utilisez plutôt une source d'alimentation prévue à cet effet (une alimentation de laboratoire).

## Le moteur unipolaire

### Connecter un moteur unipolaire

Pour rappel, voici la structure du moteur unipolaire :

![Vue en coupe d'un moteur pas-à-pas unipolaire](https://zestedesavoir.com/media/galleries/954/e8e65912-4b58-4a54-94ef-d285da528d19.png.960x960_q85.png)

Si vous avez bien lu la partie précédente, vous devez avoir retenu que ce moteur est assez simple à piloter. En effet, il suffit d'alimenter une à une les bobines pour que le moteur tourne. Et c'est tout ! Il nous faut juste utiliser le bon composant pour alimenter les bobines et en avant ! A priori, le bon composant serait un bon transistor qui pourrait supporter 50V et 500mA pour débuter. À cela il faudrait ensuite rajouter une diode de roue libre pour ne pas l’abîmer lors des phases de roue libre (tout cela multiplié par 4 puisqu'on a 4 bobines). Plutôt que de s’embêter à câbler tout ça, je vous propose l'intervention d'un nouveau composant : le ULN2003A. Ce dernier regroupe les transistors pour faire passer la puissance et aussi la diode. Il est très simple à câbler puisqu'il faut simplement amener l'alimentation et les broches de commandes. Chacune de ces dernières possède respectivement une sortie où la tension sera celle de l'alimentation. Voici une illustration de ce câblage :

![Câblage simple du moteur unipolaire - Schéma](https://zestedesavoir.com/media/galleries/954/d8ecbfb8-de09-4dfb-9192-8651d5a00c55.png.960x960_q85.jpg)

![Câblage simple du moteur unipolaire - Montage](https://zestedesavoir.com/media/galleries/954/59b23291-a16f-4cf0-966d-bf912a2a64a1.png.960x960_q85.jpg)

### Utiliser un moteur unipolaire

Je l'ai maintenant dit et répété plusieurs fois, pour ce moteur il suffit de piloter chaque bobine une à une, chacune leur tour. Je vais donc vous résumer tout cela de manière plus schématique et on sera bon pour ce moteur. :D A la fin, si vous avez bien compris vous devriez être capable de le faire tourner tout doucement en plaçant alternativement les fils In1 à 4 (abrégé In1..4) au 5V ou à la masse.

![Séquence du moteur unipolaire](https://zestedesavoir.com/media/galleries/954/1366405d-3579-4443-8503-6ed61e51bf80.png.960x960_q85.jpg)

[[i]]
| Les différentes illustrations de séquences peuvent êtres trouvées sous licence CC-BY-SA [sur wikipedia](http://fr.wikipedia.org/wiki/Moteur_pas_%C3%A0_pas) (M4RC0)

->

Etape | In 1 | In 2 | In 3 | In 4
------|------|------|------|------
Pas n°1 | HIGH | LOW | LOW | LOW
Pas n°2 | LOW | HIGH | LOW | LOW
Pas n°3 | LOW | LOW | HIGH | LOW
Pas n°4 | LOW | LOW | LOW | HIGH

Table: Séquence du moteur unipolaire

<-

![Chronogramme du moteur unipolaire](https://zestedesavoir.com/media/galleries/954/3a59d16f-e6f9-4d58-baba-c5ef811e6140.png.960x960_q85.jpg)

Si vous placez les fils de commande à la masse ou au 5V convenablement, votre moteur devrait tourner :) Vous n'avez plus qu'à créer le programme qui va bien pour piloter tout ce bazar... ben vous vous attendiez à quoi ? Une solution peut-être ? Non. :diable: Bon bon, d'accord... vous vous souvenez de vos premiers pas avec le chenillard ? Tout est dit. ;) (et si vous ne voulez pas vous fatiguer, attendez la suite du tuto :D )

## Le moteur bipolaire

### Le câbler, la théorie

Nous avons vu dans le chapitre précédent que le moteur bipolaire a besoin, à un moment, d'une inversion de courant si l'on veut pouvoir utiliser les bobines à bon escient. Vous vous souvenez probablement que nous avons vu précédemment un composant capable de faire cela : le pont en H. L'idée va donc être de câbler correctement les bobines pour pouvoir faire passer le courant dans un sens, ou dans l'autre. Je vous rappelle la structure du pont en H :

![Le pont en H - vue simplifiée](https://zestedesavoir.com/media/galleries/954/6a4d5998-633c-4e1e-9c4a-ee4155edac55.png.960x960_q85.png)

Problème, un pont en H ne peut piloter qu'une "paire de bobines" (celle qui sont dans le même axe et relié entre elles), or nous en avons deux à piloter. Heureusement, le L298 est un composant très bien pensé et ne possède non pas un mais bien deux ponts en H ! Du coup, nous pourrons en utiliser un par couple de bobines. :) Plutôt sympa non ?

### La pratique avec le L298

Dans le chapitre précédent, nous avons justement vu un composant qui possède deux ponts en H : le L298. Le reste du travail semble presque trop simple ! Pour la partie "sortie/puissance", vous devrez relier les sorties Out1 (broche 2) et Out2 (broche 3) à la première bobine et ensuite Out3 et Out4 (13 et 14) à la seconde. N'oubliez pas les diodes de roue libre (8 au total et qui supporte la puissance) ! Enfin, il ne reste qu'à connecter les entrées In1..4 à 4 entrée/sortie numérique (pas besoin de PWM). Pour l'instant, pas besoin de les relier à l'Arduino, contentez-vous de les mettre à la masse, nous allons faire un test ensemble. Comme nous voulons activer les deux ponts, mettez les deux entrées "enable" au +5V. Nous verrons dans la prochaine partie comment l'utiliser avec Arduino ;) . Voici un petit schéma récapitulatif :

![Câblage simple - Schéma](https://zestedesavoir.com/media/galleries/954/2470b115-93ae-4480-859e-c8ce4fbb472f.png.960x960_q85.png)

Le schéma de montage avec quelques condensateurs de filtrage qui viennent aider l'alimentation en cas de forte demande de courant et des condensateurs qui filtre les parasites engendrés par les bobines du moteur.

![Câblage simple - Montage](https://zestedesavoir.com/media/galleries/954/d95f7f2e-b79b-46b9-bba3-92cae69ad6e2.png.960x960_q85.png)

## Piloter le moteur bipolaire

Une fois que le moteur est branché, nous allons pouvoir le faire tourner. Comme son nom l'indique, il s'appelle pas à pas et donc nous allons le faire pivoter étape par étape et non de manière continue comme le moteur à courant continu. Cependant, en répétant les étapes de rotation rapidement et successivement, le moteur donnera l'impression de tourner sans s’arrêter entre chaque étape. Il existe trois méthodes distinctes pour piloter les moteurs bipolaires. Nous allons maintenant les voir une par une. Dans les cas qui vont suivre, on va considérer un moteur de 4 pas par tour (ce qui est ridiculement faible). Voici ce à quoi il va ressembler (comment sont placées ses bobines) :

![Vue schématisé d'un moteur pas-à-pas](https://zestedesavoir.com/media/galleries/954/32ef91e0-2e85-49ed-a878-bffacac0ce09.png.960x960_q85.png)

### Rotation par pas complet

Ce mode de fonctionnement est le plus simple, c'est pourquoi nous allons commencer par lui. Grâce à ce dernier, vous allez pouvoir faire des rotations "pas par pas". Pour cela, nous allons alternativement alimenter les bobines de droites et de gauche et inverser le sens du courant pour pouvoir faire une rotation complète du champ magnétique. Voici l'illustration Wikipédia très bien faite à ce sujet :

![Séquence à pas complet](https://zestedesavoir.com/media/galleries/954/4fa90138-e5dd-459d-a502-4bd0751ba61e.png.960x960_q85.jpg)

[[i]]
| En rouge la bobine alimentée ainsi que le sens du courant symbolisé par une flèche et en noir la bobine qui n'est pas alimentée.

En considérant que la bobine A est connectée aux entrées IN1 et IN2 et que la bobine B est connectée aux commandes IN3 et IN4, on peut donc écrire la séquence de commande suivante :

->

Etape | In 1 | In 2 | In 3 | In 4
------|------|------|------|------
Pas n°1 | HIGH | LOW | - | -
Pas n°2 | - | - | HIGH | LOW
Pas n°3 | LOW | HIGH | - | -
Pas n°4 | - | - | LOW | HIGH

Table: Séquence à pas complet

<-

(un état '-' signifie "non nécessaire", placez-le à 0V pour que la bobine soit bien inactive).


On peut traduire cette activité par le chronogramme suivant :

![Chronogramme du pilotage à pas entier](https://zestedesavoir.com/media/galleries/954/dd6c7a57-7550-480c-95d2-907084bbe3c7.png.960x960_q85.jpg)

Comme vous pouvez le voir à travers ces différents moyens d'explication, c'est somme toute assez simple. On va chercher à déplacer l'aimant central en le faisant tourner petit à petit. Pour cela on cherchera à l'attirer dans différentes positions.

### Rotation à couple maximal

Un autre mode de fonctionnement est celui dit à **couple maximal**. Cette méthode de pilotage utilise toutes les bobines à la fois pour pouvoir immobiliser au maximum l'aimant central. En effet, en utilisant plus de champs magnétiques on obtient une force supplémentaire. Par contre, on consomme évidemment d'avantage de courant. Pour comprendre ce fonctionnement, voyons les différentes étapes par un dessin puis par un chronogramme. Vous verrez, ce n'est pas très compliqué, le fonctionnement est très similaire, seules les activations de bobines changent un peu :

![Séquence à couple maximal](https://zestedesavoir.com/media/galleries/954/a2c38969-cdec-4ffd-8de4-3200f9572740.png.960x960_q85.jpg)

[[i]]
| Avez-vous remarqué quelque chose de particulier ? Dans cette utilisation, l'aimant ne fait plus face aux bobines mais se place *entre* les deux. Par contre, il effectue toujours des pas entiers, ces derniers ont juste un décalage constant par rapport à avant.

Voici le tableau correspondant au pilotage des bobines :

->

Etape | In 1 | In 2 | In 3 | In 4
------|------|------|------|------
Pas n°1 | HIGH | LOW | HIGH | LOW
Pas n°2 | HIGH | LOW | LOW | HIGH
Pas n°3 | LOW | HIGH | LOW | HIGH
Pas n°4 | LOW | HIGH | HIGH | LOW

Table: Séquence à couple maximal

<-

(un état '-' signifie "non nécessaire", placez-le à 0V pour que la bobine soit bien inactive).

![Chronogramme pour un couple max.](https://zestedesavoir.com/media/galleries/954/66ad8d65-8fa0-4363-9bd6-8b96b15a2071.png.960x960_q85.jpg)

### Rotation par demi-pas

Enfin, le dernier mode de fonctionnement est celui dit à **demi-pas**. Ce mode mélange les deux précédents puisqu'on va alterner les étapes du mode à pas complet et les étapes du mode à couple maximal. En effet, comme nous avons pu le voir dans les explications précédentes, les deux modes placent l'aimant central de manière différente. L'un est "en face des bobines" alors qu'avec l'autre est plutôt "entre les bobines". 

Ainsi, en se mettant alternativement "en face" puis "entre" les bobines on va effectuer deux fois plus de pas que précédemment puisqu'on intercalera des étapes supplémentaires. Attention, lorsque je dis "deux fois plus de pas" je veux surtout dire que l'on aura des étapes intermédiaires qui augmentent la précision du déplacement. Ce mode de pilotage est un peu plus compliqué que les précédents puisqu'il est "plus long" (8 étapes au lieu de 4) mais rien d'insurmontable vous allez voir !

![Séquence à demi-pas](https://zestedesavoir.com/media/galleries/954/1e65bea6-b6ba-47bc-ae73-58812126660c.png.960x960_q85.jpg)

->

Etape | In 1 | In 2 | In 3 | In 4
------|------|------|------|------
Pas n°1 | HIGH | LOW | - | -
Pas n°1 ½ | HIGH | LOW | HIGH | LOW
Pas n°2 | - | - | HIGH | LOW
Pas n°2 ½ | LOW | HIGH | HIGH | LOW
Pas n°3 | LOW | HIGH | - | -
Pas n°3 ½ | LOW | HIGH | LOW | HIGH
Pas n°4 | - | - | LOW | HIGH
Pas n°4 ½ | HIGH | LOW | LOW | HIGH

Table: Séquence à demi-pas

<-

(un état '-' signifie "non nécessaire", placez-le à 0V pour que la bobine soit bien inactive).

![Chronogramme du pilotage à demi-pas](https://zestedesavoir.com/media/galleries/954/0bb60cfc-b1f6-4616-9caf-a649915bdfcd.png.960x960_q85.jpg)

Maintenant que vous connaissez les différents modes de fonctionnement, vous pouvez essayer de faire tourner le moteur en branchant les entrées IN1..4 à la masse ou au 5V. Si vous le faites dans le bon ordre, votre moteur devrait tourner tout doucement, en allant d'une étape vers l'autre. :) 

[[a]]
| Si vous avez une charge qui demande trop de couple (par exemple un poids à faire monter), il peut arrive que le moteur "saute" un/des pas. Cette donnée est à prendre en compte si vous vous servez du nombre de pas effectué logiciellement comme moyen de calcul de distance.

#  Utilisation avec Arduino

## Câbler les moteurs

Avec les chapitres précédents, vous avez vu comment on devait utiliser les moteurs avec les composants gérant la puissance. Cependant, nous n'avons pas vu à quoi relier les broches de commande... Et c'est très simple ! En effet, tous les signaux sont tout ou rien et n'ont même pas besoin d'être des PWM ! Ce qui veut dire que les 4 broches de pilotage ont juste besoin d'être reliées à 4 broches numériques de la carte Arduino (2, 3, 4, 5 par exemple). Voyons ce que cela donne en schéma (qui sont exactement les mêmes que ceux de la partie précédente, mais avec une carte Arduino en plus :D )

### Le moteur unipolaire

![Câblage du moteur unipolaire - Schéma](https://zestedesavoir.com/media/galleries/954/ecca72e7-bc0d-47fc-9a98-84d225fc5d8e.png.960x960_q85.jpg)

![Câblage du moteur unipolaire - Montage](https://zestedesavoir.com/media/galleries/954/940db675-3dfc-4ffd-ac75-3dcae81d1b67.png.960x960_q85.jpg)

### Le moteur bipolaire

![Câblage du moteur bipolaire - Schéma](https://zestedesavoir.com/media/galleries/954/c2d29ced-68cc-4433-a771-cdebced314c2.png.960x960_q85.png)

![Câblage du moteur bipolaire - Montage](https://zestedesavoir.com/media/galleries/954/bc58e6e8-c4d6-4b1d-81ce-71ba9bddd906.png.960x960_q85.png)

Jusque là rien de vraiment compliqué, on passe à la suite !

## Piloter les moteurs avec Arduino

### Le principe

L'idée est toute simple, il suffit de générer la bonne séquence pour piloter les moteurs à la bonne vitesse, vous vous en doutez surement. La principale difficulté réside dans la génération des signaux dans le bon ordre afin que le moteur se déplace correctement. Bien entendu, c'est plus facile à dire qu'à faire. En effet, pour que le mouvement soit fluide, il faut que les changements dans la séquence soient faits de manière régulière et pour cela il faut une gestion du temps correcte. Ça peut sembler simple au premier abord, mais quand il s'agira de mixer le comportement du moteur avec celui du programme principal (qui devra peut-être faire des traitements assez lourds) cela deviendra probablement beaucoup moins trivial. Une bonne méthode consisterait à utiliser un **timer** sur lequel on réglerait la période à avoir qui refléterait ainsi la vitesse à obtenir. Mais avec Arduino vous allez voir que tout devient plus simple...

### L'objet Stepper

Sur Arduino les choses sont bien faites pour rester simples et accessibles, un objet a déjà été créé pour vous aider à piloter un moteur pas à pas. Attention cependant, il ne fonctionne que pour les moteurs unipolaires et bipolaires. Il tire parti du fait que ces deux types de moteur peuvent fonctionner avec une séquence commune. Ainsi, tout est généralisé et utilisable le plus simplement possible ! Ce nouveau composant s'appelle "[Stepper](http://arduino.cc/en/Reference/Stepper)". À sa création, il prend en argument le nombre de pas total que fait le moteur pour faire un tour (information trouvable dans la documentation constructeur ou empiriquement). Cette information sert à déterminer la vitesse de rotation par minute que vous pourrez ensuite régler à loisir pour faire des déplacements lents ou rapides. Il prend aussi en arguments les quatre broches servant à contrôler l'engin. Son constructeur est donc : [`Stepper(steps, pin1, pin2, pin3, pin4)`](http://arduino.cc/en/Reference/StepperConstructor). Pour initialiser le moteur, nous pouvons donc écrire la ligne suivante :

```cpp
// pour un moteur de 200 pas par tour et brancher sur les broches 2, 3, 4, 5
Stepper moteur(200, 2, 3, 4, 5);
```
Code: Initialisation d'un moteur pas à pas

Pour l'utiliser, deux fonctions sont utilisables. La première sert à définir la vitesse de rotation, exprimée en tours par minute (**trs/min**). Pour cela, on utilise la fonction [`step(steps)`](http://arduino.cc/en/Reference/StepperSetSpeed) qui prend en paramètre le nombre de pas à effectuer. Si ce nombre est négatif, le moteur tournera en sens inverse du nombre de pas spécifié. Voici un petit exemple qui va faire faire un aller-retour de 200 pas toutes les 2 secondes à votre moteur :

```cpp
#include <Stepper.h>

// pour un moteur de 200 pas par tour et brancher sur les broches 2, 3, 4, 5
Stepper moteur(200, 2, 3, 4, 5);

void setup()
{
    moteur.setSpeed(30); // 30 tours par minute
    // (rappel : ici le moteur fait 200 pas par tour,
    // on fera donc 6000 pas par minute)
}

void loop()
{
    moteur.step(1000);
    delay(100);
    moteur.step(-1000);
    delay(2000);
}
```
Code: Utilisation simple d'un moteur pas à pas

[[a]]
| La fonction `step(x)` est bloquante. Cela signifie qu'elle agit comme un délai. Tant que le moteur n'a pas fait les x pas demandés, le reste du programme est en attente.

## Aller plus loin

Vous êtes devenus incollables sur les moteurs pas à pas ? Vous en voulez encore plus ? Suffit de demander, voilà du bonus d'informations rien que pour toi cher lecteur !

### 2 fils au lieu de 4 !

On a toujours utilisé 4 fils pour commander les moteurs. C'est bien, mais que diriez-vous de sauver deux broches et de passer à seulement 2 fils au lieu de 4 ? Pas mal comme amélioration non ? Petite anecdote : un jour, un utilisateur des moteurs pàp s'est rendu compte d'un truc, dans une paire de fils pour piloter un moteur (dans la séquence utilisée par Arduino), l'information est toujours antagoniste. Si un fil est à HIGH, sa paire sera à LOW et vice versa. Du coup il suffit d'un peu d'électronique pour pouvoir inverser un des deux signaux et se retrouver ainsi avec seulement deux fils au lieu de 4 sortants d'Arduino. :)

![Moteur bipolaire avec 2 fils](https://zestedesavoir.com/media/galleries/954/ef720d06-eb16-4286-9e6d-d0c5ae2b1424.png.960x960_q85.jpg)

![Moteur unipolaire avec 2 fils](https://zestedesavoir.com/media/galleries/954/61347982-9de1-422f-b111-5f1f8751c05b.png.960x960_q85.jpg)

(source des images : [tigoe.net](http://www.tigoe.net/pcomp/code/circuits/motors/stepper-motors/)) Cette solution est plutôt intéressante du fait que les entrées/sorties sont parfois une denrée rare sur Arduino ! ^^

### Le L297

Lorsque vous utilisez votre Arduino, vous ne pouvez utiliser qu'une seule séquence. Par exemple pour un moteur bipolaire vous n'avez pas le choix entre le mode pas entier, demi-pas ou couple max. Une des solutions serait de générer vous-même les séquences. Mais c'est assez fastidieux. Une autre solution est électronique et compensera le développement informatique à faire. Un composant, nommé L297 (de la famille du L298 vous vous en doutez) est justement fait pour générer les séquences de moteur pas à pas. Il possède 4 broches de sorties pour générer la séquence et plusieurs en entrée pour "paramétrer" le fonctionnement voulu. Parmi elles on en retrouve trois principales :

+ CW/CCW : (ClockWise ou Counter ClockWise) qui décidera du sens de rotation du moteur (horaire ou antihoraire).
+ Half/Full : Qui décide si on est en mode pas entier ou demi-pas.
+ Clk : (Clock) qui est l'horloge pour la vitesse. À chaque front descendant, le moteur fera un pas.
Je vous laisse un peu chercher sur le net, vous trouverez de plus amples informations à ce sujet. Avant même de regarder sur le net, en fait, regardez plutôt sa datasheet !! ;) Un des avantages de délester le travail des séquences au L297 est que vous n'aurez plus besoin de l'objet Stepper et de sa fonction step() bloquante. Il faudra cependant toujours utiliser un composant de puissance pour laisser passer les forts courants nécessaires au moteur (comme le L298 par exemple).



