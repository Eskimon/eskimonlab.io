#!/bin/sh

python importerv2.py -c Arduino -t arduino,tuto -l tuto-arduino ./archives/arduino-premiers-pas-en-informatique-embarquee.zip
python importerv2.py -c "Arduino/I - Annexes" -t arduino,tuto -l tuto-arduino ./archives/ajouter-des-sorties-numeriques-a-larduino-le-74hc595.zip
python importerv2.py -c "Arduino/I - Annexes" -t arduino,tuto -l tuto-arduino ./archives/alimenter-une-arduino-sans-usb.zip
python importerv2.py -c "Arduino/I - Annexes" -t arduino,tuto -l tuto-arduino ./archives/gestion-de-la-memoire-sur-arduino.zip
python importerv2.py -c Arduino -t arduino -l arduino ./archives/realiser-un-telemetre-a-ultrasons.zip
python importerv2.py -c Arduino -t arduino,tp ./archives/tp-arduino-faire-une-animation-space-invaders-sur-lcd.zip
python importerv2.py -c Web -t web,tuto -l tuto ./archives/des-cartes-sur-votre-site.zip
python importerv2.py -c Web -t web,tuto -l tuto ./archives/google-maps-javascript-api-v3.zip
python importerv2.py -c Web -t web,tuto -l tuto ./archives/les-balises-audio-et-video-en-html5.zip
python importerv2.py -c Articles -t git,tuto -l tuto ./archives/refaire-lhistoire-avec-git.zip
python importerv2.py -c Articles -t python,pelican,markdown,tuto ./archives/comment-et-pourquoi-jai-integre-zmarkdown-a-pelican.zip
python importerv2.py -c Web -t python,pelican,tuto -l tuto-pelican ./archives/creer-un-site-web-statique-avec-pelican.zip
python importerv2.py -c Articles -t securite,keeweb,mot-de-passe,tuto -l tuto-keeweb ./archives/utiliser-un-gestionnaire-de-mot-de-passe.zip
python importerv2.py -c Articles -t guake,ubuntu,terminal ./archives/guake-un-terminal-drop-down-quil-est-bien.zip
python importerv2.py -c Web -t web,tuto,discord,webhook ./archives/faire-un-bot-discord-simple-avec-les-webhooks.zip
python importerv2.py -c Articles -t tuto,streaming,android -l tuto ./archives/utiliser-un-smartphone-comme-camera-de-streaming.zip

cp -R content/ ../
rm -r content/
