import argparse
import json
import os
import re
import zipfile

from collections import OrderedDict


# ZIP_PATH = '/home/eskimon/Téléchargements/gestion-de-la-memoire-sur-arduino.zip'
# ZIP_PATH = '/home/eskimon/Téléchargements/des-cartes-sur-votre-site.zip'
# ZIP_PATH = '/home/eskimon/Téléchargements/arduino-premiers-pas-en-informatique-embarquee.zip'

parser = argparse.ArgumentParser(description='ZdS archive to pelican md')
parser.add_argument('-t', '--tags', help='Tags for this content', type=str, default='')
parser.add_argument('-c', '--category', help='Category for this content', type=str, default='')
parser.add_argument('-l', '--lead', help='Leading link data', type=str, default='')
parser.add_argument('zip')
args = parser.parse_args()


ZIP_PATH = args.zip

LICENCE = 'Touts droits réservés'

ROOT_FOLDER = 'content/'
LEAD_URL = (args.lead + '-' if args.lead else '')
articles = []


def slugify(text):
    reg = r'[\u2000-\u206F\u2E00-\u2E7F\'!"#$%&()*+,./:;<=>?@[\]^`{|}~]'
    text = re.sub(reg, '', text.lower())
    text = text.strip()
    text = re.sub(r'\s+', '-', text)
    return text


class Container():

    def __init__(self, title):
        self.title = title
        self.slug = slugify(title)
        self.containers = []
        self.extracts = []
        self.depth = 1
        self.introduction = ''
        self.conclusion = ''
        self.path = ''
        self.parent = None
        self.idx = 0
        self.main_cat = ''

    def add_extract(self, extract):
        # print('adding extract "{}" to "{}"'.format(extract.title, self.title))
        self.extracts.append(extract)

    def add_container(self, container):
        container.depth = self.depth + 1
        container.parent = self
        # print('adding container "{}" to "{}"'.format(container.title, self.title))
        self.containers.append(container)

    def update_idx(self, idx):
        if self.parent:
            self.idx = self.parent.idx * 100 + idx

    def update_path(self):
        if self.parent:
            self.path = self.parent.path + '/' + self.slug

    def cleanup_slug(self):
        self.slug = str(self.idx) + '-' + self.slug

    def get_categories(self):
        cat = ''
        if self.parent:
            cat = self.parent.get_categories() + '/'
        return cat + self.title.replace('/', '-')

    def __str__(self):
        return '-' * self.depth + ' {} {} ({}) from {} (path: {})'.format(
            self.idx, self.title, len(self.containers), self.parent.title if self.parent else 'No parent', self.path)


class Extract():
    def __init__(self, title, text):
        self.title = title
        self.slug = slugify(title)
        self.text = text


def fill_containers_from_json(json_sub, parent):
    if 'children' in json_sub:
        for child in json_sub['children']:
            if child['object'] == 'container':
                new_container = Container(child['title'])
                if 'introduction' in child:
                    new_container.introduction = child['introduction']
                if 'conclusion' in child:
                    new_container.conclusion = child['conclusion']
                parent.add_container(new_container)
                if 'children' in child:
                    fill_containers_from_json(child, new_container)
            elif child['object'] == 'extract':
                new_extract = Extract(child['title'], child['text'])
                parent.add_extract(new_extract)
            else:
                raise Exception(("Type d'objet inconnu : « {} »").format(child['object']))


def extract_articles(container, idx=0):
    # Not an article
    for i, c in enumerate(container.containers):
        c.update_idx(i + 1)
        c.cleanup_slug()
        c.update_path()
        # print(c)
        extract_articles(c, c.idx)

    # Here we should be dealing only with articles...
    if container.extracts:
        container.path += '.zmd'
        # print(container.title)
        articles.append(container)


def to_zmd(zfile):
    extra_data = json.load(open('extra-data.json'))
    last_cat = ''
    last_cat_idx = 0
    for i, article in enumerate(articles):
        slug = extra_data[article.title].get('_slug', article.slug)
        # Update metadata
        meta = OrderedDict()
        meta['Title'] = article.title
        meta['Authors'] = 'Eskimon'
        meta['Date'] = '2010-12-03 10:20'  # TODO
        meta['Modified'] = '2010-12-03 10:20'  # TODO
        meta['Slug'] = LEAD_URL + slug
        meta['Tags'] = args.tags
        meta['Licence'] = LICENCE
        if article.depth > 1:
            cat = article.get_categories()
            cat = cat[0:cat.rindex("/")] if "/" in cat else cat
            # Should we increment cat letter idx?
            if not last_cat:  # First category found
                last_cat = cat
            if last_cat != cat:  # New category
                last_cat = cat
                last_cat_idx += 1
            # Append letter
            meta['Category'] = cat[0:cat.rindex("/") + 1] + chr(65 + last_cat_idx) + ' - ' + cat[cat.rindex("/") + 1:]
        else:
            meta['Category'] = article.main_cat
        meta['Previous'] = (LEAD_URL + articles[i-1].slug) if i > 0 else ''
        meta['Next'] = (LEAD_URL + articles[i+1].slug) if i < len(articles) - 1 else ''

        # Add extra-data
        try:
            for key in extra_data[article.title]:
                if not key.startswith('_'):
                    meta[key] = extra_data[article.title][key]
        except KeyError:
            pass

        # Dump articles
        content = ''
        # Meta first
        content += '\n'.join(['{}: {}'.format(m, meta[m]) for m in meta])
        content += '\n\n'
        # Then intro
        content += str(zfile.read(article.introduction), 'utf-8') + '\n\n'
        # Then toc
        # content += '\n'.join(['+ [{}](#{})'.format(e.title, e.slug) for e in article.extracts])
        # content += '\n\n'
        content += '# Sommaire\n\n'  # Automagically done with remark-toc
        # And text goes here
        for e in article.extracts:
            content += '# {}\n\n'.format(e.title)
            text = str(zfile.read(e.text), 'utf-8') + '\n\n'
            text = re.sub(r'^(#+) (.*)\n', r'#\1 \2\n', text)
            text = re.sub(r'\n(#+) (.*)\n', r'\n#\1 \2\n', text)
            # Add trailing domain for relative pics
            text = text.replace('(/media/gal', '(https://zestedesavoir.com/media/gal')
            content += text
        # And finally insert conclusion
        content += str(zfile.read(article.conclusion), 'utf-8') + '\n\n'

        # Build folder
        try:
            folder = article.path[:article.path.rindex('/')]
        except ValueError:
            folder = args.category
        folder = extra_data[article.title].get('_folder', folder)
        if not os.path.exists(ROOT_FOLDER + folder):
            os.makedirs(ROOT_FOLDER + folder)
        path = os.path.join(ROOT_FOLDER, folder, slug) + '.zmd'

        # Write article to file
        open(path, 'w').write(content)


zfile = zipfile.ZipFile(ZIP_PATH)
manifest = json.loads(str(zfile.read('manifest.json'), 'utf-8'))
LICENCE = manifest['licence']
top_parent = Container(title=manifest['title'])
top_parent.introduction = manifest['introduction']
top_parent.conclusion = manifest['conclusion']
top_parent.path = args.category
top_parent.title = args.category
top_parent.main_cat = args.category
# json to python
fill_containers_from_json(json_sub=manifest, parent=top_parent)
if len(top_parent.extracts):
    top_parent.title = manifest['title']
    top_parent.path = args.category + '/' + top_parent.slug
extract_articles(top_parent)
to_zmd(zfile)
