#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

AUTHOR = 'Eskimon'
SITENAME = "Le blog d'Eskimon"
SITEURL = 'https://eskimon.fr'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'
LOCALE = 'fr_FR.utf8'
DATE_FORMATS = '%a %d %B %Y'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = False

ARTICLE_ORDER_BY = 'basename'

ARTICLE_URL = '{slug}'
ARTICLE_SAVE_AS = '{slug}.html'
ARTICLE_LANG_URL = '{slug}-{lang}'
ARTICLE_LANG_SAVE_AS = '{slug}-{lang}.html'
DRAFT_URL = 'drafts/{slug}'
DRAFT_SAVE_AS = 'drafts/{slug}.html'
DRAFT_LANG_URL = 'drafts/{slug}-{lang}'
DRAFT_LANG_SAVE_AS = 'drafts/{slug}-{lang}.html'
PAGE_URL = '{slug}'
PAGE_SAVE_AS = '{slug}.html'
PAGE_LANG_URL = '{slug}-{lang}'
PAGE_LANG_SAVE_AS = '{slug}-{lang}.html'
CATEGORY_URL = 'category/{slug}'
CATEGORY_SAVE_AS = 'category/{slug}.html'
TAG_URL = 'tag/{slug}'
TAG_SAVE_AS = 'tag/{slug}.html'
AUTHOR_URL = 'author/{slug}'
AUTHOR_SAVE_AS = 'author/{slug}.html'
SUBCATEGORY_URL = 'subcategory/{fullurl}'
SUBCATEGORY_SAVE_AS = os.path.join('subcategory', '{savepath}.html')

# Some static files
STATIC_PATHS = ['images', 'extra/robots.txt', 'extra/CNAME', 'extra/ads.txt', 'extra/ebooks', 'extra/CV.pdf', 'extra/stream-planning.html', 'extra/commandes_AT_HC05.pdf']
EXTRA_PATH_METADATA = {
    'extra/robots.txt': {'path': 'robots.txt'},
    'extra/CNAME': {'path': 'CNAME'},
    'extra/ads.txt': {'path': 'ads.txt'},
    'extra/CV.pdf': {'path': 'CV.pdf'},
    'extra/stream-planning.html': {'path': 'stream-planning.html'},
    'extra/commandes_AT_HC05.pdf': {'path': 'commandes_AT_HC05.pdf'},
}

# Custom theme parameters
THEME = "my-theme"
THEME_STATIC_DIR = "static"

# Do not parse html files
READERS = {'html': None}

# Plugins management

PLUGIN_PATHS = ['my-plugins']
PLUGINS = ['sitemap', 'subcategory', 'zmd_reader', 'permalinks']

PERMALINK_PATH = ''

GOOGLE_ADS = True
