#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *


FEED_ALL_ATOM = 'feeds/all.atom.xml'
FEED_ALL_RSS = 'feeds/all.rss.xml'
# CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'

OUTPUT_PATH = 'public/'
DELETE_OUTPUT_DIRECTORY = True
OUTPUT_RETENTION = ['.git', '.gitignore']

# Following items are often useful when publishing

# DISQUS_SITENAME = 'eskimon-fr'
DISCORD_WIDGET = True
GOOGLE_ANALYTICS = 'UA-46353906-1'
GOOGLE_ADS = True
MONEYTIZER_ADS = False
